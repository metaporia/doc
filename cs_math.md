
---
title: 'Mathematical Foundations of Computing'
subtitle: 'Definitions, Notes, and Musings'
author: Whom? Writhes
date: July 4, 2017
tags: [math, notes, doc]
keywords: [math, notes, doc]
abstract: |
    A series of surmises.


    May they guide the way.
lang: en
papersize: a4
fontsize: 13pt
documentclass: article
classoption: onecolumn, twoside
geometry: margin=1.5in
---

# 0.0

This course covers 

    * modelling; problem solving, decomposition
    * discrete maths
        * set thery
        * proof techniques
        * induction
        * graphs
        * relations
        * functions
        * logic
    * computability
    * complexity theory

chapters 

one :  set theory, Cantor's theorem

two :   formal proof techniques

three :   mathematical induction

four :   graphs, modelling

five :   relations 

six :   set theory, rigorous treatment of the properties of infinity 

# 1.1 

set
:   a *set* is an unordered collection of distinct elements. e.g., 

    {1, 2, 3, 4}, {'cat','dog','lizard'} 
    {1, 1, 1, 1} ≡ {1}

element
:   an *element* is something contained within a set. e.g.,
    
    1 ∈ {1, 2, 3}
    {1, 2} ∈ {{1, 2}, 3} // sets may contain other sets. sets are valid
    elements 
    {1, 2} ⇔ {2, 1}



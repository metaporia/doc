# Eclipse java setup

see [arch wiki](https://wiki.archlinux.org/index.php/eclipse#Installation) for
details.

Desiderata:
- vim emulation; either eclim | viplugin | vwrapper


Download [eclim](https://github.com/ervandew/eclim/releases/download/2.8.0/eclim_2.8.0.bin).

Run the following to install eclipse and eclim for java development:

```bash
aurman -S eclipse-java
cd ~/Downloads; and cx ./eclim_2.8.0.bin; and ./eclim_2.8.0.bin
```


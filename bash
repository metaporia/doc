# input layers for 'text-mode' applications 


##  raw string input : /dev/tty
config: stty (1)
term (1), terminfo, tic
readline (for bash ...)



## ubuntu 16 

systemd -> systemd_login 
systemd_login -> tty1 w login
login -> bash
bash: readline
bash


## config source order 

for non-interactive \
    *-int. v --login \ 
    > . /etc/profile ~/.bash_profile, ~/.bash_login, ~/.profile <session> . ~/.bash_logout

for interactive non-login :
    /etc/bash.bashrc, ~/.bashrc
for scripts 
    expand $BASH_ENV, source contents
    


# Flash Ergo Dox EZ with teensy-loader-cli

1. Use [configurator](https://configure.ergodox-ez.com/layouts/default/latest/0) to
generate *.hex  layout file.

2. Run the following to flash the keyboard. Press the reset button with
pin/tack upon request.

```bash
teensy-loader-cli  -mmcu=atmega32u4 ~/Downloads/ergodox_ez_first-layout_NaVj.hex -v
```


# IR Webcam Fix (for Lenovo Thinkpad T480)

## Problem

The IR camera is the default but yields a green picture.

## Solution

1. Use `ffplay /dev/videoN` to learn the names of the two cameras.
2. Delete the offending camera (`/dev/video0`).
3. Rename the working camera to `/dev/video0`


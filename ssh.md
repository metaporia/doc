# SSH

## Add new key to SSH authentication agent

After creating the key, at `~/.ssh/id_rsa`, (1) start the authentication agent if
it's not running; (2) add the new key.

1.
```bash
eval `ssh-agent -s`
```
 or, for fish,
```bash
bass 'eval `ssh-agent -s`'
```

2.
```bash
ssh-add ~/.ssh/id_rsa
```

## Note

ssh breaks when users' respective /etc/passwd entries have invalid shells. E.g.
after reinstalling fish (formerly at /usr/bin/fish, which was in /etc/passwd),
all attempts (no matter the state of /etc/ssh/sshd_config) spat out the
abominably vague "Permission denied. Please try again."


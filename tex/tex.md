#Tex  

## Hello World

create a `hello_world.tex` file with the following contents:

    \documentclass{article}
    \begin{document}
    A \textbf{bold \textit{Hello \LaTeX}} to start!
    =end{document}

latex 'code' (macro statements, plain text) compiles w LaTeX, or other TeX engine, e.g.,

    $ pandoc hello_world.tex -o hello_world.pdf # this works


`\command`s may be nested as evinced above 


\documentclass
:   'article' | 'letter' | 'book' | 'paper'\* | 'journal'\* 
    predefined feature sets
    \*: locale specific features, see [2]

\begin
:   denote start of document body  


## 1.3 LaTeX input files

### whitespace

space 
:   "whitespace" characters; blank, tab, space, newline, linebreak

    whitespace at start of line is ignored

end of paragraph
:   one empty line

empty line
: two or more empty lines

### Special Characters

    # $ ^ & _ { } ~ \

% escape with '\' backslash


literal backslash: `\textbackslash`

### Commands

    \*cmd*[*options*]{*parameters*}

whitespace terminates.

commands eat whitespace. invoke as `\cmd{} text after whitespace!`

### Classes

    \documentclass[options]{class}

where `class` is one of: `article` | `minimal` | `report` | `book` | `slides`

`options` is one of: 

    10pt | 11pt | 12pt
    
    a4paper | letterpaper | a5paper | b5paper | executive paper | legalpaper 

    onecolumn | twocolumn

    twoside | oneside

    openright | openany

    fleqn : default centered (not left formatted) equations

    leqn : left equation numbering

### Packages

for platform/package specific packages see `local.tex`

Included Packages

* doc - documentation
* exscale - scalled math extension font
* fontenc - font encoding
* ifthen 
* latexsym - symbol fonts
* makeidx - index generation 
* syntonly - process/check syntax only, without typesetting, and .dvi output
* inputenc - specify input encoding

### Pagestyles

    \pagestyle{style}

where `style` is one of: 

    empty 

    headings: print current chapter heading and right justified page number

    plain: print chapter heading center-bottom with page number, default style 

### File Extensions

.tex 
: input files

.sty
: macro package, `\usepackage{package}`

.dtx 
: documented latex

.ins 
: installs matching .dtx file
    
.cls 
: class definitions, `\documentclass{class}`


#### latex compilation files

.log
: compile log

.toc
: TOC; headers

.lof 
: list of figures

.lot
: list of tables

.aux
: cross-reference related data compiler 'cache'

.idx
: index file á la `\makeindex`

.ind
: processed .idx (index) file 

.ilg
: `\makeindex` log

### Multi-file projects

    \include{filename}

insert contents of *filename.tex* on a new page

    \includeonly{permitted, file, names, like, these}

constrain further `\include`s to the enumerated file arguments

    \input{filename}

as `\include` except that `\input` does not insert *filename.tex* on a new page

### Syntax checking

    `\usepackage{syntonly}`
    \syntonly

after importing syntax package lint file for incorrect syntax, and command
usage



## - Typesetting Text

### Paragraph
the paragraph is the most important text element as it corresponds with a 
single thought or idea.

force line breaks with `\\`

### Sentence

note: abbreviation periods (e.g., the second '.' of 'e.g.') precede a smaller 'space' character than
sentence terminating periods.

comma
:   short stop in the flaw of language, proof-read aloud 

    "if you feel the need to breath (or make a short stop) at some other place,
    insert a comma"

### Section

    \section{The Structure of Text and Language}

does the above make sense, poor confudddled, guidance-seeking, twit?

### Linebreaks and Page Breaks
paragraph typesetting depends on document class

    \\ 
    \newline
start a newline without paragraph break

    \\*
prohibits page break after forced line break

    \\newpage
starts a new page

    \linebreak[n]
    \nolinebreak[n]

    \pagebreak[n]
    \nopagebreak[n]
where n <- [0..4], describes priority of *suggested* {page, line}breaks. The
lower n, the more decision power lies with LaTeX

    \sloppy
lower hyphenation, linebreak standards

    \fussy
reverse the effects of `\sloppy`

### Hyphenation
    `\hyphenation{word list}`
e.g., 
    `\hyphenation{FORTRAN Hy-phen-a-tion}` 
prevents 'FORTRAN' from hyphenation, and specifies optimal (syllabic, in this
case) hyphenation of 'Hyphenation'.

    \-
inserts a 'discretionary' hyphen into a word, as *the* (only) hyphenation
point. Multiple '\-'s are permissible

    `\mxbox{text}`
ensure *text* stays on one line

    `\fbox{text}`
`\mxbox` with extant outline drawn around box 


## Ready-Made Strings
    \today
current date

    \TeX, \LaTex, \LaTeXe
[La]TeX all purty, for TeX, LaTeX, LaTeX2e (epsilon, I believe)


## Special Characters

### Quotation Marks
    ``quoted text''
note backticks for opening quotation marks, vertical quote for closing marks.

### Dashes, Hyphens
    -
normal dash, hyphen

    --
en-dash: – 

    ---
em-dash: —

    $-4$
arithmetic minus, '$'s indicate math mode. '-' is decided by contextual
inference.

    $\sim$
renders "true" tilde (~)

    read/write
read as one word, slash is valid

    read\slash write
same as the above regular slash, whilst permitting hyphenation (at the '/' i
presume)


### Unicode with XeTex
1. save file as utf-8
2.  `\usepackage{polyglossia}`
    `\setdefaultlanguauge[babelshorthands]{lang A}`
3. `\usepackage[Ligatures=TeX]{fontspec}`

see ~/ws/lang/tex/unicode.tex for a unicode example that compiles (xelatex
<file.tex>)

# XeTeX vs XeLaTeX

## Xetex
* unicode support
* automatic local system font access
* quicktime (whatever that is)

## XeLaTeX
* unicode (over xetex engine)
* latex extension compatibility
* OpenType.. support
* high-level interface for OpenType features exposed by XeTeX

tldr: Xe\* tex variants allow literal input of unicode grapheme clusters

### xetex font selection
    `\font\font_alias="[fontname]{font-options}:{font-features}"{TeX
    font-features}`






































# see also #
---------------------------------------

# bib
[1]: ~/ws/doc/tex/lshort.pdf
    "The Not So Short Introduction to LaTeX2e, by Tobias Oetiker"

[2]: ~/ws/doc/tex/local.pdf

[3]: ~/ws/lang/tex/minimal.tex, ~/ws/lang/tex/minimal.pdf

# programs

`latex`
:   popular TeX engine

`xdvi`
:   \*.dvi browser, see `man xdvi`

`dvipdf`
:   convert \*.dvi to \*.pdf

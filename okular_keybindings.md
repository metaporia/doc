###Okular Keybindings
- for the given prefix, apply the nested postfix qualifiers sequentially
- do not maintain depressed prefix keystate without explicit instruction

###Prefix
- append postfix command mneumonic to ^^^ (the above conception/mneumonic) for fluid command composition

**c-c**: 'configure', 'settings', 'preferences'
    **a**: annotations
    **b**: backends
    **o**: okular; program settings
    **s**: shortcuts
    **t**: toolbars

**c-f**: 'fit', 'format', 'file view'
    **a**: auto fit doc to windows size
    **p**: fit page
    **w**: fit width
    **W**: fit window to page (note 'W', not 'w'. c-f, shift-w)
    **f**: toggle fullscreen mode
    **b**: toggle blackscreen mode
    **c**: continuous scrolling, no full page jumps
    **t**: trim margins; great for side by side work w terminal emulator
    **n**: show page number

**c-t**: 'tool', 'tooling'
    **b**: browse 
    **t**: selection
    **g**: grid selection
    **t**: text selection
    **z**: zoom

**c-s**: 'show', 'show window elements'
    **f**: forms
    **m**: menubar
    **n**: navigation panel
    **p**: page bar
    **t**: toolbar

**c-H**: 'help'
    **h**: okular handbook
    **p**: propterties
    **i**: inspect



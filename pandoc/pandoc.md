
---
title: Plain Text Workflow
author: Him Who Writhes
date: July 4, 2017
subtitle: 'Lemme Know if This Shows Up'
tags: [tex, pandoc, doc, notes]
keywords: [tex, pandoc, doc, notes]
abstract: |
    \begin{center}
    A series of surmise.


    May they guide the way.
    \end{center}
lang: en
papersize: a4
fontsize: 13pt
documentclass: article
classoption: onecolumn, twoside
geometry: margin=1.5in
---

\tableofcontents  

Pandoc Basics
---------------------------------------

See [Foo]


# Section

## Subsection
Paragraph 

Next paragraph. indents mean jack shit.

## *italics* with \*word-to-italicize\*
commentary, with footnote. [^1]

# Code Blocks
Indent by one tab | four spaces

    add :: Num a => a -> a -> a 
    add a b = a + b

## Fenced Code Blocks
Fence code blocks with three or more (>=3) '~'s.

~~~
if (a > 3) {
    moveship(5 * gravity, DOWN);
}
~~~

## Fenced Code Attributes
This pandoc markdown:

```markdown
~~~{#hask .haskell .numberLines}
add :: Num a => a -> a -> a 
add a b = a + b
~~~~
```
produces *this* latex code block:

~~~ {#hask .haskell .numberLines}
add :: Num a => a -> a -> a 
add a b = a + b
~~~~

Haskell
---

A bit of haskell for your reading pleasure, dear reader: 

~~~haskell
add' :: Num a => a -> a -> a 
add' a b = a + b
~~~~

Rust
---

For example, a nascent rustacean contrivance:

```rust
fn main() -> () {
    let x = String::from("hello world");
}
```

## Inline Code Attributes
```
`<$>`{.haskell}
```


`<$>`{.haskell}

# Verbatim
To make a short span of text verbatim put it inside backticks:
`` ` ``{.markdown}

~~~markdown
Here is a literal backtick `` ` ``.
~~~


# Line Blocks
src:

```
| Line blocks are _formatted_ in *markdown* like **this** paga
| -- oh ! what a hard wrapped line break.
|       indentation too!
|       and it persists here,
|       and <- *there*.
```

->


| Line blocks are _formatted_ in *markdown* like **this** paga
| -- oh ! what a hard wrapped line break.
|    indentation too!
|    and it persists here,
|    and <- *there*.


src2:

```markdown
| The limerick packs laughs anatomical
| In space that is quite economical.
|    But the good ones I've seen
|    So seldom are clean
| And the clean ones so seldom are comical


```
| The limerick packs laughs anatomical
| In space that is quite economical.
|    But the good ones I've seen
|    So seldom are clean
| And the clean ones so seldom are comical

# References
## Foo
This should get linked to by `See [Foo]`

# Block Quotes

> Text here in block 
> quote.

# Lists

## Bullet Lists
Use `*`,`+`,`-` as bullets, e.g.,

    * one 
    * two
    * three

compiles to:


* one 
* two
* three

  * First paragraph.

    Continued indented four spaces to indicate membership of bullet 'First
    para...'.

  * Second paragraph with block
        
        ```haskell
        id x = x
        ```
    Talks about code in self-aggrandizing fashion hereabouts.

<!-- end of list, btw this is an html comment. this text field (body) may be
elided -->

## Numbered Lists

1. Firstly, list numbering matters little,
2. Secondly, I matter little -- what a small world.

## Fancy Lists
Allows ordered list items to be marked with: uppercase, lowercase letters;
roman, arabic numerals.


#. one 
#. two
    i. two.one
    ii. Two: second element
#. three

## Definition Lists
Term 1

:   Definition 1

Term 2 with *inline markup*

:   body of *def two*, with a code example:
    
    ```haskell
    id :: a -> a 
    id x = x
    ```

    Closing remarks w.r.t. Definition 2

# Emphasis
`*`, or `_` may be used once, to provide italics, or twice, for bold.

use `_` for intra-word underscore, e.g., to empasize a single *syl*lable. 

# Strikeout
```markdown 
This is ~~is deleted text.~~
```


This is ~~is deleted text.~~

# {Super, Sub}script
```markdown
H~2~0 is  liquid.  2^10^ is 1024.
```

H~2~0 is  liquid.  2^10^ is 1024.




# Links

    <http://google.com>
    <sam@green.eggs.ham>

## Inline Links

This is an [inline link](~/dot/.vimrc), and here's [one with a title](http://fsf.org "title; click me mofo! come at me.").

# Boxes

\begin{tabular}{|p{5cm}|}
\hline
Welcome to Boxy's paragraph. 
We sincerely hop you'll 
all enjoy the show. \\
\hline
\end{tabular}

# Floating Elements
\twocolumn

\begin{table}[!tbp]
\begin{tabular}{|p{5cm}|}
\hline 
Welcome to Boxy's paragraph.
We sincerely hop you'll 
all enjoy the show. \\
\hline
\end{tabular}
\caption{yee fucking ayy maann}
\end{table}

Here is my two column work.

Second column here perhaps? No.



# Pandoc Markdown to LaTeX Command(s)

```bash
pandoc

    -fmarkdown+smart `# enable extension 'smart', curly quotes, {em, en}-dashes` \ 
    --standalone `# use FORMAT template, see ~/.pandoc/templates` \ 
    --toc `# table of contents` \ 
    --number-sections # would recommend \  
    --latex-engine=xelatex \ 
    input_file.md \ 
    -o output_file.pdf \ 
    ; mupdf output_file.pdf
```


See Also
---------------------------------------
[^1]: https://github.com/dh-notes/pandoc-workflow/blob/master/main.md
[^2]: http://pandoc.org/MANUAL.html#variables-for-latex

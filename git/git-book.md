
---
title: Git Book Notes
author: metaporia
date: July 5, 2017
abstract: |
    \begin{center}
    Versioning models information, data as it relates to time
    \end{center}
lang: en
papersize: a4
fontsize: 13pt
documentclass: article
classoption: onecolumn, twoside
geometry: margin=1.5in
---

\tableofcontents

# Get a Grip!

version control

:  a system that records changes to a set of file(s) over time.

## Concepts

### Internal Data Model
Git models data as a **stream of snapshots**.

As each cloned Git repository is a *full* backup, *most operations are local,
and therby fast*, providing ease-of-use and veritably complete *offline
capabilites*. 

Git *generally* only adds data.

### Content Addressed
SHA-1 hashes of content and metadata form a Merkle Directed Acyclic Graph
(Merkle DAG), which allows: content addressing; check-sums prior to
commit/merge/push dispatch to catch malicious, inadvertent data. In short,
content hashing mitigates information loss, corruption and tampering.

### Three States
A state is one of:

committed :
:   recent changes to *file* are stored in local repository's database\   
    (i.e., `.git` folder).

modified : 
:   *file* has uncommitted changes.

staged : 
:   *file* has changes marked for inclusion in the next commit.


A section is one of:

git directory :
:   `.git` contains metadata and object database.

working directory :
:   mirrors a single checkout of one version of the repo.

staging area :
:   a file in `.git` which stores increments of the next commit; also referred
    to as "index". 

A basic Git workflow:

1. Modify file(s) within working tree.
2. Stage files; add snapshots to staging area.
3. Commit files as represented in staging area permanently to `.git` Git
   directory.

## Post-Install Setup

`git config` stores settings at the following locations:

1. `/etc/gitconfig` file: contains values applicable to all users, repos on 
    system. configure with `--system` to `git config`
2. `~/.gitconfig` file: user specific settings; pass `--global`. 
3. `.git/config` file: repo specific settings.


## You Need Help. No, Really.
```bash
$ git help <verb>
$ git <verb> --help
$ man git-<verb>
```

e.g., `$ git help config`{.bash}

## Create a Repository
Create a repository from an existing directory:

```bash
$cd tgt_dir
$git init # creates `.git` repo skeleton
$git add *.c ## add all C src files 
$git add LICENSE 
$git commit -m 'initial project version'
```















































# References
[^1]: https://git-scm.com/book/en/v2/Getting-Started-About-Version-Control

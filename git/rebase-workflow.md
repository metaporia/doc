## How to rebase&mdash;a refresher

1. Figure out how many commits needs rejiggering.

2. Open up a 2-pane tmux split w/ `tig`, and some shell, e.g., `bash`, `fish`, `zsh`,
   etc.

3. Run the following in the shell pane
```bash
git rebase -i HEAD~$NUM_OF_COMMITS_AGO_YOU_GOOFED
```

4. Examine commits in tig, and decide how to purtify the public record of your
   feature, bugfix, etc.

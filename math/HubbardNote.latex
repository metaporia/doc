\PassOptionsToPackage{unicode=true}{hyperref} % options for packages loaded elsewhere
\PassOptionsToPackage{hyphens}{url}
%
\documentclass[13pt,english,a4paper,onecolumn, twoside]{article}
\usepackage{lmodern}
\usepackage{amssymb,amsmath}
\let\mathbbalt\mathbb
\usepackage{ifxetex,ifluatex}
\usepackage{fixltx2e} % provides \textsubscript
\ifnum 0\ifxetex 1\fi\ifluatex 1\fi=0 % if pdftex
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
  \usepackage{textcomp} % provides euro and other symbols
\else % if luatex or xelatex
  \usepackage{unicode-math}
  \defaultfontfeatures{Ligatures=TeX,Scale=MatchLowercase}
\fi
% use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
% use microtype if available
\IfFileExists{microtype.sty}{%
\usepackage[]{microtype}
\UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\IfFileExists{parskip.sty}{%
\usepackage{parskip}
}{% else
\setlength{\parindent}{0pt}
\setlength{\parskip}{6pt plus 2pt minus 1pt}
}
\usepackage{hyperref}
\hypersetup{
            pdftitle={H\&H's Vector Calculus, Linear Algebra, and Differential Forms Notes},
            pdfauthor={Keane Yahn-Krafft},
            pdfborder={0 0 0},
            breaklinks=true}
\urlstyle{same}  % don't use monospace font for urls
\usepackage[margin=1.5in]{geometry}
\setlength{\emergencystretch}{3em}  % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{0}
% Redefines (sub)paragraphs to behave more like sections
\ifx\paragraph\undefined\else
\let\oldparagraph\paragraph
\renewcommand{\paragraph}[1]{\oldparagraph{#1}\mbox{}}
\fi
\ifx\subparagraph\undefined\else
\let\oldsubparagraph\subparagraph
\renewcommand{\subparagraph}[1]{\oldsubparagraph{#1}\mbox{}}
\fi

% set default figure placement to htbp
\makeatletter
\def\fps@figure{htbp}
\makeatother

\usepackage[mathletters]{ucs}
\usepackage{amsthm}
\usepackage{multirow}
\usepackage{listings}
\usepackage{enumitem}
\DeclareMathOperator{\taninv}{{\tan}^{-1}}
\ifnum 0\ifxetex 1\fi\ifluatex 1\fi=0 % if pdftex
  \usepackage[shorthands=off,main=english]{babel}
\else
  % load polyglossia as late as possible as it *could* call bidi if RTL lang (e.g. Hebrew or Arabic)
  \usepackage{polyglossia}
  \setmainlanguage[]{english}
\fi

\title{H\&H's \emph{Vector Calculus, Linear Algebra, and Differential Forms}
Notes}
\author{Keane Yahn-Krafft}
\date{July 4, 2017}

\begin{document}
\maketitle

\tableofcontents
\clearpage

\input{ThmStyle.tex}

\newcommand{\hatb}[1]{\hat{\mathbf{#1}}}
\newcommand{\real}{\mathbbalt{R}}
\newcommand{\nat}{\mathbbalt{N}}

\newcommand{\nreal}[1]{\mathbbalt{R}^{#1}}
\newcommand{\nnat}[1]{\mathbbalt{R}^{#1}}
\renewcommand{\v}[1]{\vec{\mathbf{#1}}}
\newcommand{\p}[1]{\mathbf{#1}}
\newcommand{\SectionCounter}[1]{ \setcounter{section}{1} \setcounter{subsection}{1} \setcounter{subsubsection}{1} }

\setcounter{section}{1}
\setcounter{subsection}{1}
\setcounter{subsubsection}{1}

\hypertarget{vectors}{%
\subsection{Vectors}\label{vectors}}

\hypertarget{points-vs-vectors}{%
\subsubsection{Points vs Vectors}\label{points-vs-vectors}}

\begin{defn} Point, vector, and coordinates

The element $\mathbbalt{R}^{n}$ with coordinates $x_1,x_2,x_3,...,x_n$ can be interpreted
in two ways: as the point

\begin{center}
$\mathbf{x} = 
\begin{pmatrix}
x_1 \\
\vdots \\
x_n
\end{pmatrix}$,
or as the vector $\vec{\mathbf{x}} = \begin{bmatrix} x_1 \\ \vdots \\ x_n \end{bmatrix}$,
which represents the increment.
\end{center}

\end{defn}

\hypertarget{subtraction-and-addition-of-vectors-and-points}{%
\subsubsection{Subtraction and Addition of Vectors and
Points}\label{subtraction-and-addition-of-vectors-and-points}}

\begin{defn} Vectors are added by "zipping" together corresponding
elements and adding each pair. Take vectors $\vec{\mathbf{v}},\vec{\mathbf{w}} ∈ \mathbbalt{R}^{n}$.
Let

\begin{center}
$\vec{\mathbf{v}} = \begin{bmatrix} v_1 \\ v_2 \\ \vdots \\ v_n \end{bmatrix}$ and 
$\vec{\mathbf{w}} = \begin{bmatrix} w_1 \\ w_2 \\ \vdots \\ w_n \end{bmatrix}$, so
$\vec{\mathbf{v}} + \vec{\mathbf{w}} = 
\begin{bmatrix} v_1 + w_1 \\ v_2 + w_2 \\ \vdots \\ v_n + w_n \end{bmatrix}$
\end{center}


Note: This is very much like that of the following haskell snippet:
\begin{lstlisting}[language=Haskell]
addVector :: [a] -> [a] -> [a]
addVector = zipWith (+)
\end{lstlisting}
\end{defn}

\hypertarget{scalar-multiplication-of-vectors}{%
\subsubsection{Scalar Multiplication of
Vectors}\label{scalar-multiplication-of-vectors}}

\begin{defn}
Let vector $\vec{\mathbf{v}} ∈ \mathbbalt{R}^{n}$ and $r ∈ \mathbbalt{R}$. Then 

$a\begin{bmatrix} x_1 \\ x_2 \\ \vdots \\ x_n \end{bmatrix} =
\begin{bmatrix} ax_1 \\ ax_2 \\ \vdots \\ ax_n \end{bmatrix}$.

Theoretically the contents of the vectors, and the scalar itself, 
could be parameterized, as long as the replacement is a field.
\end{defn}

\hypertarget{subspaces-of-mathbbaltrn}{%
\subsubsection{\texorpdfstring{Subspaces of
\(\mathbbalt{R}^{n}\)}{Subspaces of \textbackslash{}mathbbalt\{R\}\^{}\{n\}}}\label{subspaces-of-mathbbaltrn}}

\begin{defn} Subspace of $\mathbbalt{R}^{n}$

A non-empty subset $V ∈ \mathbbalt{R}^{n}$ is called a \emph{subspace} if it is closed
under addition (with itself) and closed under multiplication by scalars; that
is, $V$ is a \emph{subspace} if when

\begin{center}
$\vec{\mathbf{x}},\vec{\mathbf{y}} ∈ V$, and $a ∈ \mathbbalt{R}$, \qquad then \qquad $\vec{\mathbf{x}} + \vec{\mathbf{y}} ∈ V$ and
$a\vec{\mathbf{x}} ∈ V$.
\end{center}

\end{defn}

\hypertarget{standard-basis-vectors}{%
\subsubsection{Standard Basis Vectors}\label{standard-basis-vectors}}

\begin{defn} Standard Basis Vectors in $\mathbbalt{R}^{n}$

The \emph{standard basis vectors} in $\mathbbalt{R}^{n}$ are the vectors $\vec{\mathbf{e_j}}$ with
$n$ entries, the $j$th entry 1 and the others 0.

For example, in $\mathbbalt{R}^{3}$ they are denoted $\vec{\mathbf{i}}, \vec{\mathbf{j}}, \vec{\mathbf{k}}$ or
$\hat{\mathbf{i}}, \hat{\mathbf{j}}, \hat{\mathbf{k}}$, and defined as follows:

\begin{center}
$\vec{\mathbf{i}} = \vec{\mathbf{e_1}} = \begin{bmatrix} 1 \\ 0  \\ 0 \end{bmatrix};\quad
\vec{\mathbf{j}} = \vec{\mathbf{e_2}} = \begin{bmatrix} 0 \\ 1 \\ 0 \end{bmatrix};\quad
\vec{\mathbf{k}} = \vec{\mathbf{e_3}} = \begin{bmatrix} 0 \\ 0 \\ 1 \end{bmatrix}$.
\end{center}

\end{defn}

\hypertarget{vector-fields}{%
\subsubsection{Vector Fields}\label{vector-fields}}

\begin{defn} Vector Field

A vector field on $\mathbbalt{R}^{n}$ is a function whose input is a point in
$\mathbbalt{R}^{n}$ and whose output is a vector in $\mathbbalt{R}^{n}$ emanating from that
point.
\end{defn}

\setcounter{subsection}{2}
\setcounter{subsubsection}{1}

\hypertarget{matrices}{%
\subsection{Matrices}\label{matrices}}

\begin{defn} Matrix
An $m \times n$ matrix is a rectangular array of entries, $m$ high and $n$
wide. We denote by Mat($m,n$) the set of $m \times n$ matrices.

Remember: a matrix's dimensions have the form $rows \times columns$.

\end{defn}

\hypertarget{multiplication}{%
\subsubsection{Multiplication}\label{multiplication}}

\begin{defn} Matrix Multiplication

If $A$ is an $m \times n$ matrix whose $(i,j)$th entry is $a_{ij}$, and $B$ is
an $n \times p$ matrix whose $(i,j)$th entry is $b_{ij}$, then $C = AB$ is the
$m \times p$ matrix with entries

\begin{center}
$$c_{ij} = \sum_{k=1}^n a_{ik}b_{kj} = a_{i1}b_{1j} + a_{i2}b_{2j} + ... + a_{in}b_{nj}$$.
\end{center}
\end{defn}

\hypertarget{multiplication-by-basis-vectors}{%
\subsubsection{Multiplication by Basis
Vectors}\label{multiplication-by-basis-vectors}}

\begin{defn} The $i$th column of matrix $A$ is $A\vec{\mathbf{e_i}}$; that is, 
multiplying a matrix by the $i$th basis vector, $\vec{\mathbf{e_i}}$, returns the $i$th
column.
\end{defn}

\begin{thm} \textbf{Matrix multiplication is associative:} 
If $A$ is an $n \times m$ matrix, $B$ is an $m \times p$ matrix, and $C$ is a
$p \times q$ matrix, so that $(AB)C$ and $A(BC)$ are both defined, then they
are equal: $$(AB)C = A(BC)$$.

\end{thm}

\hypertarget{multiplicative-identity}{%
\subsubsection{Multiplicative Identity}\label{multiplicative-identity}}

\begin{defn} The Identity Matrix

The identity matrix $I_n$ is the $n \times n$ matrix with 1's along the main
diagonal (from top left to bottom right) and 0's elsewhere.

For example,

\begin{center}
$I_2 = \begin{bmatrix} 1 & 0 \\ 0 & 1 \end{bmatrix}$.
\end{center}

If $A$ is an $n \times m$ matrix, then
\begin{center}
$IA = AI = A$, \qquad or, more precisely, \qquad $I_nA = AI_m = A$.
\end{center}
\end{defn}

\hypertarget{inverses}{%
\subsubsection{Inverses}\label{inverses}}

\begin{defn} Left and Right Inverses of Matrices

Let $A$ be a matrix. If there is another matrix $B$ such that 
$$BA = I,$$
then $B$ is called a left inverse of $A$. Similarly, if there is another matrix
$C$ such that
$$AC=I,$$
then $C$ is called the right inverse of $A$.

It is possible for a nonzero matrix to have neither a right nor a left inverse.

There exists a formula for calculating the inverse of a $2 \times 2$ matrix:
the inverse of

\begin{center}

$A = \begin{bmatrix} a & b \\ c & d \end{bmatrix}$ \quad is \quad
$A^{-1} = \dfrac{1}{ad - bc}\begin{bmatrix} d & -b \\ -c & a \end{bmatrix}$.
\end{center}

\end{defn}

\begin{defn} Invertible Matrix

An \emph{invertible matrix} is a matrix that has both a left and a right
inverse, in which case they are equal. 
\end{defn}

\begin{thm} \textbf{The inverse of the product of matrices:} 
If $A$ and $B$ are invertible matrices, then $AB$ is invertible, and the
inverse is given by the formula $$(AB)^{-1} = B^{-1}A^{-1}$$.

\end{thm}

\hypertarget{the-transpose}{%
\subsubsection{The Transpose}\label{the-transpose}}

\begin{defn} 
The transpose $A^T$ of a matrix $A$ is formed by interchanging all the rows and
columns of A, reading the rows from the left to right, and columns from top to
bottom.

For example, if 
$A = \begin{bmatrix} a & b & c \\ e & f & g \end{bmatrix}$,
then
$A^T = \begin{bmatrix} a & e \\ b & f \\ c & g \end{bmatrix}$.

More formally, the $i$th row, the $j$th column of $A$ is the $j$th row, the
$i$th column of $A^T$. For all row indices $i,j$

$$ [A]_{ij} = [A^T]_{ji}$$.

\end{defn}

\begin{thm} \textbf{The transpose of a product:} The transpose of a product is
the product of the transposes in reverse order:
$$(AB)^T = B^T A^T$$.
\end{thm}

\hypertarget{special-kinds-of-matrices}{%
\subsubsection{Special Kinds of
Matrices}\label{special-kinds-of-matrices}}

\begin{defn} A \emph{symmetric matrix} is equal to its
transpose. An anti-symmetric matrix is equal to minus its transpose.
That is, given a matrix $A$: 
\begin{enumerate}[label=(\roman*)]
\item if $A$ is symmetric, then $A = A^T$; and
\item if $A$ is anti-symmetric, then $A = -A^T$.
\end{enumerate}

For example, 
$\begin{bmatrix} 1 & 1 & 0 \\ 1 & 0 & 3 \\ 0 & 3 & 0 \end{bmatrix}$
is symmetric, and
$\begin{bmatrix} 0 & 1 & 2 \\ -1 & 0 & 3 \\ -2 & -3 & 0 \end{bmatrix}$
is anti-symmetric
\end{defn}

\begin{defn} An \emph{upper triangular matrix} is a square matrix with  nonzero
entries only on or above the main diagonal. A \emph{lower triangular matrix} is
a square matrix with nonzero entries only on or below the main diagonal.
\end{defn}

\begin{defn} A \emph{diagonal matrix} is a square matrix with nonzero
entries (if any0 only on the main diagonal.

For example, 
$\begin{bmatrix} 1 & 0 & 0 \\  0 & 1 & 0 \\ 0 & 0 & 1 \end{bmatrix}$
is a diagonal matrix.
\end{defn}

\setcounter{subsection}{3}
\setcounter{subsubsection}{1}

\hypertarget{mappings}{%
\subsubsection{Mappings}\label{mappings}}

\begin{defn} A \emph{mapping} associates elements of one set to another.  A
mapping, $f$, from $X$ to $Y$ is denoted $f: X \to Y$.  To be well defined a
mapping must be defined at every point of the domain, X, and for each, must
return a unique element of the range or codomain, Y. The image of the domain is
the subset of the range, $Y$, whose elements are mapped to.

More formally, $f: X \to Y$ means:

\begin{enumerate}[label=(\roman*)]
\item $f ⊆ X \times Y$; that is, $f$ is a subset of the Cartesian product of
$X$ and $Y$.

\item The domain of $f$ is $X$.

\item For all $x ∈ X$, if $(x, y_0), (x, y_1) ∈ f$ then $y_0 = y_1$.
\end{enumerate}
\end{defn}

\begin{defn} A mapping is \emph{surjective} (or \emph{onto}) if every element
of the set of arrival corresponds to at least one element of the set of departure.
\end{defn}

\begin{defn} A mapping is \emph{injective} (or \emph{one to one}) if
every element of the set of arrival corresponds to at most one element of the
set of departure.
\end{defn}

\begin{defn} A mapping is \emph{bijective} if it is both onto and
one to one. A bijective mapping is invertible.
\end{defn}

\begin{defn} The \emph{composition} $f \circ g$ of two mappings, $f$ and $g$,
    is $$(f \circ g)(x) = f(g(x))$$.
\end{defn}

\begin{thm} \textbf{Composition is associative} That is to say:
    $$f \circ g \circ h = (f \circ g) \circ h = f \circ (g \circ h)$$.
\end{thm}

\hypertarget{linearity}{%
\subsubsection{Linearity}\label{linearity}}

\begin{defn} A \emph{linear transformation} $T : \mathbbalt{R}^{n} \to \mathbbalt{R}^{m}$ is a
    mapping such that for all scalars $a,b$ and all $\vec{\mathbf{v}}, \vec{\mathbf{w}} ∈ \mathbbalt{R}^{n}$,
    $$T(a\vec{\mathbf{v}}+b\vec{\mathbf{w}}) = aT(\vec{\mathbf{v}}) + bT(\vec{\mathbf{w}})$$.
\end{defn}

\end{document}



---
author: Keane Yahn-Krafft
date: July 4, 2017
lang: en
papersize: a4
fontsize: 13pt
title: H&H's _Vector Calculus, Linear Algebra, and Differential Forms_ Notes
documentclass: article
classoption: onecolumn, twoside
geometry: margin=1.5in
header-includes: |
    \usepackage[mathletters]{ucs}
    \usepackage{amsthm}
    \usepackage{multirow}
    \usepackage{listings}
    \usepackage{enumitem}
    \usepackage{mathtools}
    \DeclareMathOperator{\taninv}{{\tan}^{-1}}

---

\tableofcontents
\clearpage

\input{ThmStyle.tex}

\newcommand{\hatb}[1]{\hat{\mathbf{#1}}}
\newcommand{\real}{\mathbbalt{R}}
\newcommand{\nat}{\mathbbalt{N}}
\newcommand\deq{\mathrel{\stackrel{\makebox[0pt]{\mbox{\normalfont\tiny def}}}{=}}}

\makeatletter
\renewcommand*\env@matrix[1][\arraystretch]{%
  \edef\arraystretch{#1}%
  \hskip -\arraycolsep
  \let\@ifnextchar\new@ifnextchar
  \array{*\c@MaxMatrixCols c}}
\makeatother

\newcommand{\nreal}[1]{\mathbbalt{R}^{#1}}
\newcommand{\nnat}[1]{\mathbbalt{R}^{#1}}
\renewcommand{\v}[1]{\vec{\mathbf{#1}}}
\newcommand{\p}[1]{\mathbf{#1}}
\newcommand{\SectionCounter}[1]{ \setcounter{section}{1} \setcounter{subsection}{1} \setcounter{subsubsection}{1} }

# Chapter 1
\setcounter{subsection}{1}
\setcounter{subsubsection}{1}
## Vectors
### Points vs Vectors
\begin{defn} Point, vector, and coordinates

The element $\nreal{n}$ with coordinates $x_1,x_2,x_3,...,x_n$ can be interpreted
in two ways: as the point

\begin{center}
$\p{x} = 
\begin{pmatrix}
x_1 \\
\vdots \\
x_n
\end{pmatrix}$,
or as the vector $\v{x} = \begin{bmatrix} x_1 \\ \vdots \\ x_n \end{bmatrix}$,
which represents the increment.
\end{center}

\end{defn}

### Subtraction and Addition of Vectors and Points

\begin{defn} Vectors are added by "zipping" together corresponding
elements and adding each pair. Take vectors $\v{v},\v{w} ∈ \nreal{n}$.
Let

\begin{center}
$\v{v} = \begin{bmatrix} v_1 \\ v_2 \\ \vdots \\ v_n \end{bmatrix}$ and 
$\v{w} = \begin{bmatrix} w_1 \\ w_2 \\ \vdots \\ w_n \end{bmatrix}$, so
$\v{v} + \v{w} = 
\begin{bmatrix} v_1 + w_1 \\ v_2 + w_2 \\ \vdots \\ v_n + w_n \end{bmatrix}$
\end{center}


Note: This is very much like that of the following haskell snippet:
\begin{lstlisting}[language=Haskell]
addVector :: [a] -> [a] -> [a]
addVector = zipWith (+)
\end{lstlisting}
\end{defn}


### Scalar Multiplication of Vectors

\begin{defn}
Let vector $\v{v} ∈ \nreal{n}$ and $r ∈ \real$. Then 

$a\begin{bmatrix} x_1 \\ x_2 \\ \vdots \\ x_n \end{bmatrix} =
\begin{bmatrix} ax_1 \\ ax_2 \\ \vdots \\ ax_n \end{bmatrix}$.

Theoretically the contents of the vectors, and the scalar itself, 
could be parameterized, as long as the replacement is a field.
\end{defn}

### Subspaces of $\nreal{n}$

\begin{defn} Subspace of $\nreal{n}$

A non-empty subset $V ∈ \nreal{n}$ is called a \emph{subspace} if it is closed
under addition (with itself) and closed under multiplication by scalars; that
is, $V$ is a \emph{subspace} if when

\begin{center}
$\v{x},\v{y} ∈ V$, and $a ∈ \real$, \qquad then \qquad $\v{x} + \v{y} ∈ V$ and
$a\v{x} ∈ V$.
\end{center}

\end{defn}

### Standard Basis Vectors

\begin{defn} Standard Basis Vectors in $\nreal{n}$

The \emph{standard basis vectors} in $\nreal{n}$ are the vectors $\v{e_j}$ with
$n$ entries, the $j$th entry 1 and the others 0.

For example, in $\nreal{3}$ they are denoted $\v{i}, \v{j}, \v{k}$ or
$\hatb{i}, \hatb{j}, \hatb{k}$, and defined as follows:

\begin{center}
$\v{i} = \v{e_1} = \begin{bmatrix} 1 \\ 0  \\ 0 \end{bmatrix};\quad
\v{j} = \v{e_2} = \begin{bmatrix} 0 \\ 1 \\ 0 \end{bmatrix};\quad
\v{k} = \v{e_3} = \begin{bmatrix} 0 \\ 0 \\ 1 \end{bmatrix}$.
\end{center}

\end{defn}

### Vector Fields

\begin{defn} Vector Field

A vector field on $\nreal{n}$ is a function whose input is a point in
$\nreal{n}$ and whose output is a vector in $\nreal{n}$ emanating from that
point.
\end{defn}

\setcounter{subsection}{2}
\setcounter{subsubsection}{1}
## Matrices

\begin{defn} Matrix
An $m \times n$ matrix is a rectangular array of entries, $m$ high and $n$
wide. We denote by Mat($m,n$) the set of $m \times n$ matrices.

Remember: a matrix's dimensions have the form $rows \times columns$.

\end{defn}

### Multiplication
\begin{defn} Matrix Multiplication

If $A$ is an $m \times n$ matrix whose $(i,j)$th entry is $a_{ij}$, and $B$ is
an $n \times p$ matrix whose $(i,j)$th entry is $b_{ij}$, then $C = AB$ is the
$m \times p$ matrix with entries

\begin{center}
$$c_{ij} = \sum_{k=1}^n a_{ik}b_{kj} = a_{i1}b_{1j} + a_{i2}b_{2j} + ... + a_{in}b_{nj}$$.
\end{center}
\end{defn}


### Multiplication by Basis Vectors
\begin{defn} The $i$th column of matrix $A$ is $A\v{e_i}$; that is, 
multiplying a matrix by the $i$th basis vector, $\v{e_i}$, returns the $i$th
column.
\end{defn}


\begin{thm} \textbf{Matrix multiplication is associative:} 
If $A$ is an $n \times m$ matrix, $B$ is an $m \times p$ matrix, and $C$ is a
$p \times q$ matrix, so that $(AB)C$ and $A(BC)$ are both defined, then they
are equal: $$(AB)C = A(BC)$$.

\end{thm}

### Multiplicative Identity
\begin{defn} The Identity Matrix

The identity matrix $I_n$ is the $n \times n$ matrix with 1's along the main
diagonal (from top left to bottom right) and 0's elsewhere.

For example,

\begin{center}
$I_2 = \begin{bmatrix} 1 & 0 \\ 0 & 1 \end{bmatrix}$.
\end{center}

If $A$ is an $n \times m$ matrix, then
\begin{center}
$IA = AI = A$, \qquad or, more precisely, \qquad $I_nA = AI_m = A$.
\end{center}
\end{defn}


### Inverses

\begin{defn} Left and Right Inverses of Matrices

Let $A$ be a matrix. If there is another matrix $B$ such that 
$$BA = I,$$
then $B$ is called a left inverse of $A$. Similarly, if there is another matrix
$C$ such that
$$AC=I,$$
then $C$ is called the right inverse of $A$.

It is possible for a nonzero matrix to have neither a right nor a left inverse.

There exists a formula for calculating the inverse of a $2 \times 2$ matrix:
the inverse of

\begin{center}

$A = \begin{bmatrix} a & b \\ c & d \end{bmatrix}$ \quad is \quad
$A^{-1} = \dfrac{1}{ad - bc}\begin{bmatrix} d & -b \\ -c & a \end{bmatrix}$.
\end{center}

\end{defn}

\begin{defn} Invertible Matrix

An \emph{invertible matrix} is a matrix that has both a left and a right
inverse, in which case they are equal. 
\end{defn}

\begin{thm} \textbf{The inverse of the product of matrices:} 
If $A$ and $B$ are invertible matrices, then $AB$ is invertible, and the
inverse is given by the formula $$(AB)^{-1} = B^{-1}A^{-1}$$.

\end{thm}

### The Transpose

\begin{defn} 
The transpose $A^T$ of a matrix $A$ is formed by interchanging all the rows and
columns of A, reading the rows from the left to right, and columns from top to
bottom.

For example, if 
$A = \begin{bmatrix} a & b & c \\ e & f & g \end{bmatrix}$,
then
$A^T = \begin{bmatrix} a & e \\ b & f \\ c & g \end{bmatrix}$.

More formally, the $i$th row, the $j$th column of $A$ is the $j$th row, the
$i$th column of $A^T$. For all row indices $i,j$

$$ [A]_{ij} = [A^T]_{ji}$$.

\end{defn}

\begin{thm} \textbf{The transpose of a product:} The transpose of a product is
the product of the transposes in reverse order:
$$(AB)^T = B^T A^T$$.
\end{thm}

### Special Kinds of Matrices

\begin{defn} A \emph{symmetric matrix} is equal to its
transpose. An anti-symmetric matrix is equal to minus its transpose.
That is, given a matrix $A$: 
\begin{enumerate}[label=(\roman*)]
\item if $A$ is symmetric, then $A = A^T$; and
\item if $A$ is anti-symmetric, then $A = -A^T$.
\end{enumerate}

For example, 
$\begin{bmatrix} 1 & 1 & 0 \\ 1 & 0 & 3 \\ 0 & 3 & 0 \end{bmatrix}$
is symmetric, and
$\begin{bmatrix} 0 & 1 & 2 \\ -1 & 0 & 3 \\ -2 & -3 & 0 \end{bmatrix}$
is anti-symmetric
\end{defn}


\begin{defn} An \emph{upper triangular matrix} is a square matrix with  nonzero
entries only on or above the main diagonal. A \emph{lower triangular matrix} is
a square matrix with nonzero entries only on or below the main diagonal.
\end{defn}

\begin{defn} A \emph{diagonal matrix} is a square matrix with nonzero
entries (if any0 only on the main diagonal.

For example, 
$\begin{bmatrix} 1 & 0 & 0 \\  0 & 1 & 0 \\ 0 & 0 & 1 \end{bmatrix}$
is a diagonal matrix.
\end{defn}


\setcounter{subsection}{3}
\setcounter{subsubsection}{1}
### Mappings

\begin{defn} A \emph{mapping} associates elements of one set to another.  A
mapping, $f$, from $X$ to $Y$ is denoted $f: X \to Y$.  To be well defined a
mapping must be defined at every point of the domain, X, and for each, must
return a unique element of the range or codomain, Y. The image of the domain is
the subset of the range, $Y$, whose elements are mapped to.

More formally, $f: X \to Y$ means:

\begin{enumerate}[label=(\roman*)]
\item $f ⊆ X \times Y$; that is, $f$ is a subset of the Cartesian product of $X$ and $Y$.

\item The domain of $f$ is $X$.

\item For all $x ∈ X$, if $(x, y_0), (x, y_1) ∈ f$ then $y_0 = y_1$.
\end{enumerate}
\end{defn}

\begin{defn} A mapping is \emph{surjective} (or \emph{onto}) if every element
of the set of arrival corresponds to at least one element of the set of departure.
\end{defn}

\begin{defn} A mapping is \emph{injective} (or \emph{one to one}) if
every element of the set of arrival corresponds to at most one element of the
set of departure.
\end{defn}

\begin{defn} A mapping is \emph{bijective} if it is both onto and
one to one. A bijective mapping is invertible.
\end{defn}


\begin{defn} The \emph{composition} $f \circ g$ of two mappings, $f$ and $g$,
    is $$(f \circ g)(x) = f(g(x))$$.
\end{defn}

\begin{thm} \textbf{Composition is associative} That is to say:
    $$f \circ g \circ h = (f \circ g) \circ h = f \circ (g \circ h)$$.
\end{thm}


### Linearity

\begin{defn} A \emph{linear transformation} $T : \nreal{n} \to \nreal{m}$ is a
    mapping such that for all scalars $a,b$ and all $\v{v}, \v{w} ∈ \nreal{n}$,
    $$T(a\v{v}+b\v{w}) = aT(\v{v}) + bT(\v{w})$$.
\end{defn}

\begin{thm} \textbf{Matrices and linear transformations: }
    \begin{enumerate}
        \item 
            Any $m \times n$ matrix $A$ defines a linear transformation $T:
            \nreal{n} \to \nreal{m}$ by matrix multiplication: $$T(\v{v}) =
            A\v{v}.$$
        \item 
            Every linear transformation $T : \nreal{n} \to \nreal{m}$ is given
            by the multiplication by the $m \times n$ matrix $[T]$: $$T(\v{v}) =
            [T]\v{v},$$ where the $i$th column of $[T]$ is $T(\v{e_i})$.
    \end{enumerate}
\end{thm}


\begin{defn} \emph{Reflection with $2 \times 2$ matrices}

    Let $T: \nreal{2} \to \nreal{2}$ be a linear transformation that reflects a
    point over some line $\theta$ radians from the $x$-axis. Then its
    associated (reflection) matrix, $M$, is defined as follows:

    \begin{center}
        $M = 
        \begin{bmatrix} 
            \cos 2\theta & \sin 2\theta \\
            \sin 2\theta & -\cos 2\theta
        \end{bmatrix}
        =
        \begin{bmatrix}
            T(\v{e_1}) & T(\v{e_2}) 
        \end{bmatrix}$.
    \end{center}
\end{defn}


\begin{defn} \emph{Rotation by an angle $\theta$}

    A matrix $R$ giving a rotation by $\theta$ counterclockwise about the
    origin is linear, and that its matrix is

    \begin{center}
    $[R(\v{e_1}),R(\v{e_2})] = 
        \begin{bmatrix}
            \cos \theta & -\sin \theta \\
            \sin \theta & \cos \theta 
        \end{bmatrix}$.
    \end{center}
\end{defn}

\begin{thm} \textbf{Composition corresponds to matrix multiplication: }
    Suppose $S: \nreal{n} \to \nreal{m}$ and $T: \nreal{n} \to \nreal{m}$ are
    linear transformations given by the matrices $[S]$ and $[T]$ respectively.
    Then the composition $T \circ S$ is linear and
                    $$[T \circ S] = [T][S].$$
\end{thm}

\begin{thm} \textbf{Invertibility of matrices and linear transformations: }

    A linear transformation $T: \nreal{n} \to \nreal{m}$ is invertible if and
    only if the $m \times n$ matrix $[T]$ is invertible. If it is invertible,
    then 
            $$[T^{-1}] = [T]^{-1}.$$
\end{thm}

\begin{defn} \emph{Affine function, affine subspace}

    A function $\v{f} : \nreal{n} \to \nreal{m}$ is \emph{affine} if the
    function $\v{x} \mapsto \v{f}(\v{x}) - \v{f}(\v{0})$ is linear. The image
    of an affine function is called an \emph{affine subspace}.

\end{defn}

\clearpage
## The geometry of $n$-dimensional real space

### The dot product
\begin{defn} \emph{Dot product}

The \emph{dot product} $\v{x} \cdot \v{y}$ of two vectors $\v{x}, \v{y} ∈
\nreal{n}$ is 

$$ \v{x} \cdot \v{y} =
\begin{bmatrix}
    x_1 \\
    x_2 \\
    \vdots \\
    x_n \\
\end{bmatrix}
  \cdot
\begin{bmatrix}
    y_1 \\
    y_2 \\
    \vdots \\
    y_n \\
\end{bmatrix} 
\deq x_1y_1 + x_2y_2 + ... + x_ny_n.$$
\end{defn}

\begin{defn} \emph{The length of a vector}

    The \emph{length} $|\v{x}|$ of a vector $\v{x} ∈ \nreal{n}$ is
    $$ |\v{x}| \deq 
        \sqrt{\v{x} \cdot \v{x}} = \sqrt{ x_1^2 + x_2^2 + ... + x_n^2}.$$
\end{defn}


\begin{thm} \textbf{Geometric interpretation of dot product}

    Let $\v{x}, \v{y}$ in $\nreal{2}$ or $\nreal{3}$, and let $\alpha$ be the angle
    between them. Then
            $$\v{x} \cdot \v{y}  =  |\v{x}| |\v{y}| \cos \alpha.$$
\end{thm}

\begin{defn} \emph{The angle between two vectors}

    The \emph{angle} between two vectors $\v{v},\v{w} ∈ \nreal{n}$ is that
    angle $\alpha$ satisfying $0 ≤ \alpha ≤ π$ such that
            $$\cos \alpha  = \dfrac{\v{x} \cdot \v{y}}{|\v{x}| |\v{y}|}.$$

    Note that as a result 
        $$\alpha  = \arccos \dfrac{\v{x} \cdot \v{y}}{|\v{x}| |\v{y}|}.$$
\end{defn}

\begin{thm} \textbf{The triangle inequality: } For vectors $\v{x}, \v{y} ∈ \nreal{n}$,
    $$|\v{x} + \v{y}| ≤ |\v{x}| + |\v{y}|.$$
\end{thm}

\begin{defn} \emph{The length of a matrix}

    If $A$ is an $m \times n$ matrix, its length $|A|$ is the square root of
    the sum of the squares of all its entries:
    $$|A|^2 \deq \sum_{i=1}^n\sum_{j=1}^m a_{i,j}^2.$$
\end{defn}

\begin{thm} \textbf{Lengths of products: } Let $A$ be an $m \times n$ matrix,
    $B$ an $m \times k$ matrix, and $\v{b}$ a vector in $\nreal{m}$. Then
    \begin{align}
        |A\v{b}| & ≤ |A||\v{b}| \\
        |AB| & ≤ |A||B|
    \end{align}
\end{thm}
\begin{defn} \textbf{Trace in $\nreal{n}$}

    The \emph{trace} tr of A an $n \times n$ matrix is the sum of its diagonal
    entries:
            \[\text{tr } A \deq \sum_{i=1}^n a_{n,n}.\]
\end{defn}

\begin{defn} \textbf{Determinant in $\nreal{2}$}

    The \emph{determinant} det of a $2 \times 2$ matrix 
    $\begin{bmatrix} a_1 & b_1 \\ a_2 & b_2 \end{bmatrix}$ is given by
        \begin{center}
            det $\begin{bmatrix} a_1 & b_1 \\ a_2 & b_2 \end{bmatrix}
                    \deq a_1b_2 - a_2b_2.$
        \end{center}
\end{defn}

\begin{defn} \emph{Determinant in $\nreal{3}$}

The \emph{determinant} of a $3 \times 3$ matrix is defined by the rule

    \begin{center}
        det $\begin{bmatrix}
          a_1 & b_1 & c_1 \\
          a_2 & b_2 & c_2 \\
          a_3 & b_3 & c_3 
        \end{bmatrix} \deq
        a_1$ det$ 
        \begin{bmatrix}
          b_2 & c_2 \\
          b_3 & c_3
        \end{bmatrix}
        - b_1$ det$
        \begin{bmatrix}
          a_2 & c_2 \\
          a_3 & c_3
        \end{bmatrix}
        + c_1$ det$
        \begin{bmatrix}
          a_2 & b_2 \\
          a_3 & b_3
        \end{bmatrix}$.
    \end{center}
\end{defn}

\begin{defn} \textbf{The cross product in $\nreal{3}$}

    The \emph{cross product} $\v{a} \times \v{b}$ in $\nreal{3}$ is
    \begin{center}
        $\begin{bmatrix} a_1 \\ a_2 \\ a_3 \end{bmatrix} \times
        \begin{bmatrix} b_1 \\ b_2 \\ b_3 \end{bmatrix} \deq
        \begin{bmatrix}[1.4]
                \phantom{-}$det $ 
                \begin{bmatrix}
                  a_2 & b_2 \\
                  a_3 & b_3
                \end{bmatrix} \\
                - $det $ 
                \begin{bmatrix} 
                  a_1 & b_1 \\
                  a_3 & b_3
                \end{bmatrix} \\
                \phantom{-}$det $ 
                \begin{bmatrix}
                  a_2 & b_2 \\
                  a_3 & b_3
                \end{bmatrix}
        \end{bmatrix} =
        \begin{bmatrix*}[r]
            a_2b_3 - a_3b_2 \\ 
           -a_1b_3 + a_3b_1 \\ 
            a_1b_2 - a_2b_1 
        \end{bmatrix*}
        $
    \end{center}
\end{defn}

\begin{thm} The cross product and the determinant satisfy
    \[\emph{det }[\v{a}, \v{b}, \v{c}] = \v{a} \cdot (\v{b} \times \v{c}).\]
\end{thm}



---
author: Keane Yahn-Krafft
date: July 4, 2017
lang: en
papersize: a4
fontsize: 13pt
documentclass: article
classoption: onecolumn, twoside
geometry: margin=1.5in
header-includes: |
    \usepackage[mathletters]{ucs}
    \usepackage{amsthm}
    \usepackage{multirow}
    \DeclareMathOperator{\taninv}{{\tan}^{-1}}

---

\tableofcontents

\newtheorem{theorem}{Theorem}
\theoremstyle{definition}
\newtheorem{definition}{Definition}
\newtheorem{corrolary}{Corrolary}
\newtheorem*{remark}{Remark}


\newcommand{\hatb}[1]{\hat{\mathbf{#1}}}

\newcommand{\real}{\mathbbalt{R}}
\newcommand{\nat}{\mathbbalt{N}}

\newcommand{\nreal}[1]{\mathbbalt{R}^{#1}}
\newcommand{\nnat}[1]{\mathbbalt{R}^{#1}}

# Properties from T&M's _Vector Calculus_

## 1.2 The Inner Product, Length, and Distance

### Point Direction Form of a Line

The equation of the line $l$ through the tip of
a and pointing in the direction of the vector $v$ is $l(t) = a + tv$, where the parameter
$t$ takes on all real values. In coordinate form, the equations are

\begin{center}
$x = x_1 + at$,

$y = y_1 + bt$,

$z = z_1 + ct$,
\end{center}

### Parametric Equation of a Line: Point--Point Form 

The parametric equations of the line $l$ through the points 
$P = (x_1 , y_1 , z_1 )$ and $Q = (x_2 , y_2 , z_2 )$ are

\begin{center}
$x = x_1 + (x_2 − x_1 )t$,

$y = y_1 + ( y_2 − y_1 )t$,

$z = z_1 + (z_2 − z_1 )t$,
\end{center}

where $(x, y, z)$ is the general point of $l$, and the parameter $t$ takes on all real values.


### Inner Product, Length, and Distance

Letting $a = a_{1}\hatb{i} + a_{2}\hatb{j} + a_{3}\hatb{k}$ and
$b = b_{1}\hatb{i} + b_{2}\hatb{j} + b_{3}\hatb{k}$, their _inner product_ is
\begin{center}
$a · b = a_{1}b_{1} + a_{2}b_{2} + a_{3}b_{3}$,
\end{center}
while the _length_ of a is
\begin{center}
$\|a\| = \sqrt{a · a} = \sqrt{a^{2}_{1} + a^{2}_{2} + a^{2}_{3}}$.
\end{center}


To _normalize_ a vector $a$, form the vector

\begin{center}
$\dfrac{a}{\|a\|}$.
\end{center}

The _distance_ between the endpoints of $a$ and $b$ is $a−b$, and the _distance between_
$P$ and $Q$ is 

\begin{center}
$\|\vec{PQ}\|$.
\end{center}

\vspace{4mm}

### The Angle Between Two Vectors

\begin{theorem}
Let $a$ and $b$ be two vectors in $\mathbbalt{R}^3$ and let $θ$, where $0 ≤ θ ≤ π$, be
the angle between them. Then

\begin{center}
$a · b = \|a\| \|b\| \cosθ $.
\end{center}
\end{theorem}

\vspace{16mm}

\begin{corrolary} \textbf{\emph{Cauchy–Schwarz Inequality}}
For any two vectors a and b, we
have

\begin{center}
$|a · b| ≤ \|a\| \|b\|$
\end{center}

with equality if and only if a is a scalar multiple of b, or one of them is 0.
\end{corrolary}


### Orthogonal Projection

The _orthogonal projection_ of $\vec{v}$ on $\vec{a}$ is the vector

\begin{center}
$\vec{p} = \dfrac{\vec{a} \cdot \vec{v}}{{\|\vec{a}\|}^2} a$
\end{center}

The _length_ of p is

\begin{center}
$\|\vec{p}\| = \dfrac{|\vec{a}\cdot\vec{b}|}{\|\vec{a}\|^2}\|\vec{a}\|$.
\end{center}

\vspace{6mm}

\begin{theorem} \textbf{The Triangle Inequality}

For vectors $a$ and $b$ in space,

\begin{center}
$\|a + b\| ≤ \|a\| + \|b\|$
\end{center}
\end{theorem}


## 1.3 Matrices, Determinants, and the Cross Product



### $2 \times 2$ Matrices

We define a $2 \times 2$ _matrix_ to be an array

\begin{center}
$\begin{bmatrix}
  a_{11} & b_{12}  \\
  d_{12} & e_{22}  \\
\end{bmatrix}$,
\end{center}

where $a_{11}$, $a_{12}$, $a_{21}$, and $a_{22}$ are four scalars. The _determinant_

\begin{center}
$\begin{vmatrix}
  a_{11} & b_{12}  \\
  d_{12} & e_{22}  \\
\end{vmatrix}$
\end{center}

of such a matrix is the real number defined by the equation

\begin{center}
$\begin{vmatrix}
  a_{11} & a_{12}  \\
  a_{21} & a_{22}  \\
\end{vmatrix}= a_{11}a_{22} - a_{12}a_{21}$.
\end{center}


### $3 \times 3$ Matrices

We define a $3 \times 3$ _matrix_ to be an array

\begin{center}
$\begin{bmatrix}
  a_{11} & a_{12} & a_{13} \\
  a_{21} & a_{22} & a_{23} \\
  a_{31} & a_{32} & a_{33} \\
\end{bmatrix}$,
\end{center}
where each $a_{ij}$ is a scalar; $a_{ij}$ denotes the entry in the array that
is in the $i$th row and the $j$th column. The _determinant_ of a $3 \times 3$
matrix is defined by the rule

\begin{center}
$\begin{vmatrix}
  a_{11} & a_{12} & a_{13} \\
  a_{21} & a_{22} & a_{23} \\
  a_{31} & a_{32} & a_{33} \\
\end{vmatrix}= 
a_{11}
\begin{vmatrix}
  a_{22} & a_{23} \\
  a_{32} & a_{33} \\
\end{vmatrix}
- a_{12}
\begin{vmatrix}
  a_{21} & a_{23} \\
  a_{31} & a_{33} \\
\end{vmatrix}
+ a_{13}
\begin{vmatrix}
  a_{21} & a_{22} \\
  a_{31} & a_{32} \\
\end{vmatrix}$.
\end{center}

### Properties of Determinants

Interchange of rows or columns results in a change of sign. Defined as follows 
for $2 \times 2$ determinants: for rows, 

\begin{center}
$\begin{vmatrix}
  a_{11} & a_{12}  \\
  a_{21} & a_{22}  \\
\end{vmatrix} = a_{11}a_{22} - a_{21}a_{12} = -(a_{21}a_{12} - a_{11}a_{22}) =
-
\begin{vmatrix}
  a_{21} & a_{22}  \\
  a_{11} & a_{12}  \\
\end{vmatrix}$
\end{center}

and for columns,

\begin{center}
$\begin{vmatrix}
  a_{11} & a_{12}  \\
  a_{21} & a_{22}  \\
\end{vmatrix} = -(a_{12}a_{21} - a_{11}a_{22}) = - 
\begin{vmatrix}
  a_{12} & a_{11}  \\
  a_{22} & a_{21}  \\
\end{vmatrix}$.
\end{center}

This property also holds for $3 \times$ matrices.


Scalars can be factored out of any row or colum. For $2 \times 2$ determinants,
we have

\begin{center}
$\begin{vmatrix}
  αa_{11} & a_{12}  \\
  αa_{21} & a_{22}  \\
\end{vmatrix} = 
\begin{vmatrix}
  a_{11} & αa_{12}  \\
  a_{21} & αa_{22}  \\
\end{vmatrix} = α
\begin{vmatrix}
  a_{11} & a_{12}  \\
  a_{21} & a_{22}  \\
\end{vmatrix} = 
\begin{vmatrix}
  αa_{11} & αa_{12}  \\
  a_{21} & a_{22}  \\
\end{vmatrix} = 
\begin{vmatrix}
  a_{11} & a_{12}  \\
  αa_{21} & αa_{22}  \\
\end{vmatrix}$.
\end{center}

Similarly, for $3 \times 3$ determinants we have

\begin{center}

$\begin{vmatrix}
  αa_{11} & αa_{12} & αa_{31} \\
  a_{21} & a_{22} & a_{23} \\
  a_{31} & a_{32} & a_{33} \\
\end{vmatrix}= 
\begin{vmatrix}
  a_{11} & a_{12} & a_{13} \\
  αa_{21} & αa_{22} & αa_{23} \\
  a_{31} & a_{32} & a_{33} \\
\end{vmatrix}= 
\begin{vmatrix}
  a_{11} & a_{12} & a_{13} \\
  a_{21} & a_{22} & a_{23} \\
  αa_{31} & αa_{32} & αa_{33} \\
\end{vmatrix}= 
α
\begin{vmatrix}
  a_{11} & a_{12} & a_{13} \\
  a_{21} & a_{22} & a_{23} \\
  a_{31} & a_{32} & a_{33} \\
\end{vmatrix}= 
\begin{vmatrix}
  αa_{11} & a_{12} & a_{13} \\
  αa_{21} & a_{22} & a_{23} \\
  αa_{31} & a_{32} & a_{33} \\
\end{vmatrix}= 
\begin{vmatrix}
  a_{11} & αa_{12} & a_{13} \\
  a_{21} & αa_{22} & a_{23} \\
  a_{31} & αa_{32} & a_{33} \\
\end{vmatrix}= 
\begin{vmatrix}
  a_{11} & a_{12} & αa_{13} \\
  a_{21} & a_{22} & αa_{23} \\
  a_{31} & a_{32} & αa_{33} \\
\end{vmatrix}$.
\end{center}

\vspace{6mm}

\begin{remark}
By the above properties, if any row or column consists solely of
zeros, then the value of the determinant is zero. This is because we can factor
out the zero, whereupon we apply the redoubtable $0 \cdot x = 0$.
\end{remark}


### Cross Products

\begin{definition} \textbf{\emph{The Cross Product}}

Suppose that $\vec{a} = a_1\hatb{i} + a_2\hatb{j} + a_3\hatb{k}$ and $\vec{b} = b_1\hatb{i}
+ b_2\hatb{j} + b_3\hatb{k}$ are vectors in ${\mathbbalt{R}}^3$. 

The \emph{cross product} or \emph{vector product} of $\vec{a}$ and $\vec{b}$, 
denoted $\vec{a} \times \vec{b}$, is defined to be the vector

\begin{center}
$\vec{a} \times \vec{b} =
\begin{vmatrix}
  \hatb{i} & \hatb{j} & \hatb{j} \\
  a_1 & a_2 & a_3 \\
  b_1 & b_2 & b_3 \\
\end{vmatrix} =
\begin{vmatrix}
  a_2 & a_3 \\
  b_2 & b_3 \\
\end{vmatrix} 
\hatb{i} -
\begin{vmatrix}
  a_1 & a_3 \\
  b_1 & b_3 \\
\end{vmatrix} 
\hatb{j} + 
\begin{vmatrix}
  a_1 & a_2 \\
  b_1 & b_2 \\
\end{vmatrix} 
\hatb{k}$.
\end{center}
\end{definition}
\vspace{6mm}

\begin{definition} \textbf{\emph{The Cross Product}}

\emph{Geometric Definition} : $\vec{a} \times \vec{b}$ is the vector such that:

\begin{enumerate}
\item{
$\|\vec{a} \times \vec{b}\| = \|\vec{a}\| \|\vec{b}\| \sin θ$, the area of
the parallelogram defined by $\vec{a}$ and $\vec{b}$; $0 ≤ θ ≤ \pi$ 
is the angle between $\vec{a}$ and $\vec{b}$.}

\item{
$\|\vec{a} \times \vec{b}\|$ is perpendicular to $\vec{a}$ and $\vec{b}$, and
the triple $(\vec{a}, \vec{b}, \vec{a} \times \vec{b})$ obeys the right-hand
rule.}
\end{enumerate}

\emph{Component Formula}:
\begin{center}
$\vec{a} \times \vec{b} =
\begin{vmatrix}
  \hatb{i} & \hatb{j} & \hatb{j} \\
  a_1 & a_2 & a_3 \\
  b_1 & b_2 & b_3 \\
\end{vmatrix} = 
(a_2b_3 - a_3b_2)\hatb{i} - (a_1b_3 - a_3b_1)\hatb{j} + (a_1b_2 a_2b_1)\hatb{k}$.
\end{center} 

\emph{Alegbraic Rules}:
\begin{enumerate}

\item $\vec{a} \times \vec{b}$ iff $\vec{a}$ and $\vec{b}$ are parallel or
$\vec{a}$ or $\vec{b}$ is zero.

\item $\vec{a} \times \vec{b}  -\vec{b} \times \vec{a}$

\item $\vec{a} \times (\vec{b} \times \vec{c}) =  \vec{a} \times \vec{b} + \vec{a} \times \vec{c}$

\item $(\vec{a} \times \vec{b}) \times \vec{c} =  \vec{a} \times \vec{c} + \vec{b} \times \vec{c}$

\item $(α\vec{a}) \times \vec{b} = α (\vec{a} \times \vec{b})$

\end{enumerate}

\emph{Multiplication Table}:


\centering
\def\arraystretch{1.5}
$\begin{tabular}{cc|rrr}
 \multirow{2}{*}{}
 & & \multicolumn{3}{l}{Second factor} \\
 & $\times$               & $\hatb{i}$ & $\hatb{j}$ & $\hatb{k}$  \\ 
\cline{2-5}
 \multirow{3}{*}{First factor}
 & $\hatb{i}$             & $\hatb{0}$  & $\hatb{k}$   & $\hatb{-j}$   \\
 & $\hatb{j}$             & $\hatb{-k}$ & $\hatb{0}$   & $\hatb{i}$   \\
 & $\hatb{k}$             & $\hatb{j}$  & $\hatb{-i}$  & $\hatb{0}$ \\
\end{tabular}$

\end{definition}

\vspace{6mm}

\begin{remark} \emph{Geometries of Determinants}

$2 \times 2$: The absolute value of the determinant of two vectors $\vec{a}$ and $\vec{b}$ 
is the area of the parallelogram whose adjacent sides are the vectors 
$\vec{a} = a_1\hatb{i} + a_2\hatb{j}$ and $\vec{b} = b_1\hatb{i} +
b_2\hatb{j}$. The determinant is positive when rotating counterclockwise and
the angle between $\vec{a}$ and $\vec{b}$ is less than $\pi$.

$3 \times 3$: A similar geometric interpretation holds for a $3 \times 3$ matrix, the
absolute value of the determinant of which is the volume of the parallelpiped
whose adjacent sides are the vectors contained in each row of the matrix, as
with a $2 \times 2$'s.
\end{remark}


### Planes

\begin{definition} \textbf{\emph{Equation of a Plane in Space}}

The equation of the plane $\mathscr{P}$ through $(x_0, y_0, z_0)$ that has a
normal vector $\vec{n} = A\hatb{i} + B\hatb{j} + C\hatb{k}$ is

\begin{center}
$A(x - x_0) + B(y - y_0) + C(z - z_0) = 0;$
\end{center}

that is, $(x,y,z) ∈ \mathscr{P}$ if and only if

\begin{center}
$Ax + By + Cz + D = 0$,
\end{center}

where $D = -Ax_0 - By_0 - Cz_0$.
\end{definition}
\vspace{6mm}

\begin{definition} \textbf{\emph{Unit Normal Vector}}

The \emph{unit normal vector}, $\vec{n}$, of plane $\mathscr{P}$ with equation 
$Ax + By + Cz + D = 0$ is

\begin{center}
$\vec{n} = \dfrac{A\hatb{i} + B\hatb{j} + C\hatb{k}}{\sqrt{A^2 + B^2 + c^2}}$.
\end{center}

\end{definition}
\vspace{6mm}

\begin{definition} \textbf{\emph{Distance from a Point to a Plane}}

The distance from $(x_1, y_1, z_1)$ to the plane $Ax + By + Cz + D = 0$ is

\begin{center}
$Distance = \dfrac{|Ax_1 + By_1 + Cz_1 + D|}{\sqrt{A^2 + B^2 + C^2}}$
\end{center}
\end{definition}

## 1.4 Cylindrical Coordinates

\begin{definition} 

The \emph{cylindrical coordinates} $(r,θ,z)$ of a point $(x,y,z)$
are defined 

\begin{center}
$x  = r \cos θ,  \qquad y  = r \sin θ,  \qquad z  = z$.
\end{center}

To express $r$, $θ$, and $z$ in terms of $x$, $y$, and $z$, we have

\begin{center}

$r  = \sqrt{x^2 + y^2}, \qquad
θ  = \begin{cases} 
  \taninv (y/x),     & \text{ if } x > 0 \text{ and } y ≥ 0 \\ 
  π + \taninv (y/x), & \text{ if } x < 0 \\
  2π + \taninv (y/x) & \text{ if } x > 0 \text{ and } y < 0
\end{cases}$
\end{center}
\end{definition}

\vspace{4mm}

\begin{definition}

The \emph{spherical coordinates} of points $(x,y,z)$ in space are the triples
$(ρ, θ, φ)$, defined as follows:

\begin{center}
$x = ρ \sin φ \cos θ, \qquad y = ρ \sin φ \sin θ, \qquad z = ρ \cos φ$,
\end{center}

where

\begin{center}
$ρ ≥ 0, \qquad 0 ≤ θ < 2π, \qquad 0 ≤ φ ≤ π$.
\end{center}
\end{definition}


## 1.5 $n$-dimensional Euclidean Space

### Vectors in $n$-space

\begin{theorem} \textbf{Properties of the Inner Product in $\mathbbalt{R}^n$}

For $\vec{x}, \vec{y}, \vec{z} ∈ \mathbbalt{R}^n$ and $α$, $β$,
real numbers, we have

\begin{enumerate}

\item $(α\vec{x} + β\vec{y}) \cdot \vec{z} = α(\vec{x} \cdot \vec{z}) + β(\vec{y} \cdot \vec{z})$

\item $\vec{x} \cdot \vec{y} = \vec{y} \cdot \vec{x}$

\item $\vec{x} \cdot \vec{x} ≥ 0$

\item $\vec{x} \cdot \vec{x} ≥ 0$ iff $\vec{x} = \vec{0}$

\end{enumerate}
\end{theorem}

\vspace{4mm}

\begin{theorem} \textbf{Cauchy–Schwarz Inequality in $\mathbbalt{R}^n$}

Let $\vec{x}$, $\vec{y}$ be any two vectors in $\mathbbalt{R}^n$. Then

\begin{center}
$|\vec{x} · \vec{y}| ≤ \|\vec{x}\| \|\vec{y}\|$
\end{center}

with equality if and only if $\vec{x}$ is a scalar multiple of $\vec{y}$, or one of them is $\vec{0}$.
\end{theorem}

\vspace{4mm}

\begin{theorem}  \textbf{Triangle Inequality in $\mathbbalt{R}^n$}

Let $\vec{x}$, $\vec{y}$ be any two vectors in $\mathbbalt{R}^n$. Then

\begin{center}
$ \| \vec{x} + \vec{y} \| ≤ \|\vec{x}\| \|\vec{y}\|$.
\end{center}
\end{theorem}

\vspace{4mm}

\begin{definition} Matrix Multiplication

Let $A = [a_{ij}]$, an $m \times n$ matrix, and $B = [b_{ij}]$, an $n \times o$
matrix. Then the elements of $AB = C$, an $m \times p$ matrix, are given by

\begin{center}
$$c_{ij} = \sum_{k=1}^n a_{ik}b_{kj}$$,
\end{center}

which is the dot product of the $i$th row of $A$ and the $j$th column of $B$.

\end{definition}

## 2.1 The Geometry of Real Valued-Functions

### Level Curves, Surfaces, and Sections

\begin{definition} Level Curves and Surfaces

Let $f: U ⊂ \nreal{n} \to \real$ and let $c ∈ \real$. Then the \emph{level set}
of value $c$ is defined to be the set of those points $\vec{x} ∈ U$ at which
$f(\vec{x}) = c$. If $n = 2$ we speak of a \emph{level curve}; and if $n = 3$,
we speak of a \emph{level surface}.  Symbolically, the level set of value c is
written

\begin{center}
$\{ \vec{x} ∈ U \mid f(\vec{x}) = c \} ⊂ \nreal{n}$.
\end{center}

Note that $\vec{x}$ is always an element of the domain of $f$; that is, $U$.

\end{definition}

\vspace{4mm}

\begin{definition} A \emph{section} of the graph $f$ is an intersection of the
graph and a vertical plane.  
\end{definition}

## 2.2 Limits and Continuity

\begin{definition} Open Disk (or Ball)

Let $x_0 ∈ \nreal{n}$ and let real number $r > 0$. The \emph{open disk (or
ball)} of radius $r$ and center $x_0$ is defined to be the set of all points
such that $\|x - x_0\| < r$, and denoted $D_r(x_0)$.

\end{definition}

\vspace{4mm}

\begin{definition} Open Set

Let $U ⊂ \nreal{n}$. $U$ is an open set when for every point $x_0$ in $U$ there
exists some $r > 0$ such that $D_r(x_0)$ is contained within $U$, or,
symbolically, $D_r(x_0) ⊂ U$.

\end{definition}

\vspace{4mm}

\begin{definition} Neighborhood

Let $x ∈ \nreal{n}$. A \emph{neighborhood} of $x$ is some open set $U$ that
contains $x$. 
\end{definition}

\vspace{4mm}

\begin{definition} Boundary Points

Let $A ⊂ \nreal{n}$. A point $x ∈ \nreal{n}$ is called \emph{boundary point} of
$A$ if every neighborhood of $x$ has contains at least one point within $A$ and
one $without$.

Note that if $x$ itself isn't an element of $A$, then $x$ is boundary point of $A$
if every neighborhood of $x$ contains at least one point in $A$ (which case
applies for the boundaries of open sets). The
converse applies when $x$ \emph{is} in $A$.
\end{definition}


---
author: Keane Yahn-Krafft
date: July 4, 2017
lang: en
papersize: a4
fontsize: 13pt
documentclass: article
classoption: onecolumn, twoside
geometry: margin=1.5in
header-includes: |
    \usepackage[utf8x]{inputenc}
    \usepackage[mathletters]{ucs}
    \usepackage{cancel}
    \usepackage{bussproofs}
---

Sequent and Natural Deduction Rules
---------------------------------------

Sequent Rule (Axiom Rule)
: 

If $ψ ∈ Γ$ then the sequent $(Γ \vdash ψ)$ is correct.

\vspace{1mm}

Sequent Rule (Transitive Rule) 
: 

If $(∆ \vdash ψ)$ is correct and for every $δ$ in $∆$, $(Γ \vdash δ)$
is correct, then $(Γ \vdash ψ)$ is correct.

\vspace{1mm}

Natural Deduction Rule (Axiom Rule)
: 

Let $φ$ be a statement. Then $φ$ is a derivation. Its conclusion is $φ$, 
and it has one undischarged assumption, namely $φ$.

\vspace{1mm}

## AND

Sequent Rule $(∧I)$
: 

If $(Γ \vdash φ)$ and $(∆ \vdash ψ)$ are correct sequents then $(Γ∪∆ \vdash
(φ ∧ ψ))$ is a correct sequent.

\vspace{1mm}

Natural Deduction Rule ($∧I$) 
: 

If
\AxiomC{$D$}
\noLine
\UnaryInfC{$\phi$}
\DisplayProof and
\AxiomC{$D\prime$}
\noLine
\UnaryInfC{$ψ$}
\DisplayProof
are derivations of $\phi$ and $\psi$ respectively, then
\begin{prooftree}
    \AxiomC{D}
    \noLine
    \UnaryInfC{$\phi$}

    \AxiomC{D'}
    \noLine
    \UnaryInfC{$\psi$}
\RightLabel{$(∧I)$}
\BinaryInfC{$\phi\wedge\psi$}
\end{prooftree}
is a derivation of $(\phi ∧ ψ)$. Its undischarged assumptions are those of $D$ together
with those of $D\prime$.

\vspace{1mm}

Sequent Rule ($∧E$) 
: 

If the sequent $(Γ \vdash (φ ∧ ψ))$ is correct, then so are both
the sequents $(Γ \vdash φ)$ and $(Γ \vdash ψ)$.

\vspace{1mm}

Natural Deduction Rul ($∧E$)
: 

If 

\AxiomC{$D$} \noLine
\UnaryInfC{$(\phi\wedge\psi$}
\DisplayProof 
is a derivation of $(\phi ∧ \psi)$, then
\AxiomC{$D$} \noLine
\UnaryInfC{$(\phi\wedge\psi$}
\RightLabel{$(∧E)$}
\UnaryInfC{$\phi$}
\DisplayProof 
and
\AxiomC{$D$} \noLine
\RightLabel{$(∧E)$}
\UnaryInfC{$(\phi\wedge\psi$}
\UnaryInfC{$\psi$}
\DisplayProof 
are derivations of $\phi$ and $\psi$, respectively. Their undischarged
assumptions are those of $D$.

## IMPLICATION
Natural Deduction Rule ($→I$) 
: 

Suppose
\AxiomC{$D$}
\noLine
\UnaryInfC{$ψ$}
\DisplayProof 
is a derivation of $ψ$, and $φ$ is a statement. Then the following is a derivation of
$(φ → ψ)$:
\AxiomC{$\cancel\psi$} \noLine
\UnaryInfC{$D$} \noLine
\UnaryInfC{$ψ$}
\RightLabel{$(→I)$}
\UnaryInfC{$(φ → ψ)$}
\DisplayProof

Its undischarged assumptions are those of $D$, except possibly $φ$.
We can also express ($→I$) as a sequent rule:

Sequent Rule ($→I$) 
: 

If the sequent $(Γ ∪ {φ} \vdash ψ)$ is correct then so is the
sequent $(Γ \vdash (φ → ψ))$.

\vspace{1mm}

Natural Deduction Rule (→E)
: 

If 
\AxiomC{$D$} \noLine
\UnaryInfC{$ψ$}
\DisplayProof 
and
\AxiomC{$D\prime$} \noLine
\UnaryInfC{$(\phi → \psi)$}
\DisplayProof
are derivations of $\phi$ and $(\phi → \psi)$, respectively, then

\AxiomC{$D$} \noLine
\UnaryInfC{$ψ$}

\AxiomC{$D\prime$} \noLine
\UnaryInfC{$(\phi → \psi)$}

\RightLabel{$(→E)$}
\BinaryInfC{$\psi$}
\DisplayProof
is a derivation of $\psi$. Its undischarged assumptions are those of $D$
together with those of $D\prime$.

\vspace{1mm}

Sequent Rule $(→E)$ 
: 

If $(Γ \vdash φ)$ and $(∆ \vdash (φ → ψ))$ are both correct sequents,
then the sequent $(Γ ∪ ∆ \vdash ψ)$ is correct.

Natural Deduction Rule $(↔I)$
: 

If 
\AxiomC{$D$} \noLine
\UnaryInfC{$(\phi→\psi)$}
\DisplayProof 
and
\AxiomC{$D\prime$} \noLine
\UnaryInfC{$(\psi→\phi)$}
\DisplayProof 
are derivations of $(\phi → \psi)$ and $(\psi → \phi)$, respectively, then

\AxiomC{$D$} \noLine
\UnaryInfC{$(\phi→\psi)$}
\AxiomC{$D\prime$} \noLine
\UnaryInfC{$(\psi→\phi)$}
\BinaryInfC{$(\phi ↔ \psi)$}
\DisplayProof
is a derivation of $(\phi ↔ \psi)$. Its undischarged assumptions are those of
$D$ together with those of $D\prime$.

\vspace{1mm}

Natural Deduction Rule $(↔ E)$
: 

If 
\AxiomC{$D$} \noLine
\UnaryInfC{$(\phi ↔ \psi)$}
\DisplayProof 
is a derivation of $(\phi ↔ \psi)$, then
\AxiomC{$D$} \noLine
\UnaryInfC{$(\phi ↔ \psi)$}
\RightLabel{$(↔ E)$}
\UnaryInfC{$(\phi → \psi)$}
\DisplayProof
and
\AxiomC{$D\prime$} \noLine
\UnaryInfC{$(\phi ↔ \psi)$}
\RightLabel{$(↔ E)$}
\UnaryInfC{$(\psi → \phi)$}
\DisplayProof
are derivations of $(\phi → \psi)$ and $(\psi → \phi)$, respectively. Their
undischarged assumptions are those of $D$.

\vspace{1mm}

### NOT

Natural Deduction Rule $(¬E)$
: 

If
\AxiomC{$D$} \noLine
\UnaryInfC{$\phi$}
\DisplayProof 
and
\AxiomC{$D\prime$} \noLine
\UnaryInfC{$¬\phi$}
\DisplayProof 
are derivations of $\phi$ and $(¬\phi)$ respectively, then
\AxiomC{$D$} \noLine
\UnaryInfC{$\phi$}
\AxiomC{$D\prime$} \noLine
\UnaryInfC{$¬\phi$}
\RightLabel{$(¬E)$}
\BinaryInfC{$\bot$}
\DisplayProof
is a derivation of $\bot$. Its undischarged assumptions are those of $D$
together with $D\prime$.

\vspace{1mm}

Natural Deduction Rule $(¬I)$
: 

Suppose 
\AxiomC{$D$} \noLine \UnaryInfC{$\bot$} \DisplayProof
is a derivation of $\bot$, and $\bot$ is a statement. Then the following is a
derivation of $(¬\phi)$:

\begin{prooftree}
\AxiomC{$\cancel\psi$} \noLine
\UnaryInfC{$D$} \noLine
\UnaryInfC{$\bot$}
\RightLabel{$(¬I)$}
\UnaryInfC{$(¬\phi)$}
\end{prooftree}

Its undischarged assumptions are those of $D$, except possibly $\phi$.

\vspace{1mm}

Natural Deduction Rule $(RAA)$
: 

Suppose we have derivation
\AxiomC{$D$} \noLine \UnaryInfC{$\bot$} \DisplayProof
whose conclusion is $\bot$. Then there is a derivation
\begin{prooftree}
\AxiomC{$\cancel{(¬\phi)}$} \noLine
\UnaryInfC{$D$} \noLine
\UnaryInfC{$\bot$}
\RightLabel{$(RAA)$}
\UnaryInfC{$\phi$}
\end{prooftree}

Its undischarged assumptions are those of $D$, except possibly $(¬\phi)$.

\vspace{1mm}

### OR
Sequent Rule $(∨I)$ 
: 

If at least one of $(Γ \vdash φ)$ and $(Γ \vdash ψ)$ is a correct sequent,
then the sequent $(Γ \vdash (φ ∨ ψ))$ is correct.

Natural Deduction Rule $(∨I)$
: 

If
\AxiomC{$D$} \noLine \UnaryInfC{$\phi$} \DisplayProof
is a derivation with conclusion $\phi$, then
\begin{prooftree}
\AxiomC{D} \noLine \UnaryInfC{$\phi$}
\UnaryInfC{$(\phi ∨ \psi)$}
\end{prooftree}
is a derivation of $(\phi ∨ \psi)$. Its undischarged assumptions are those of $D$. 
Similarly, if
\AxiomC{$D$} \noLine \UnaryInfC{$\psi$} \DisplayProof
is a derivation with conclusion $\psi$, then
\begin{prooftree}
\AxiomC{$D$} \noLine \UnaryInfC{$\psi$}
\UnaryInfC{$(\phi ∨ \psi)$}
\end{prooftree}
is a derivation with conclusion $(\phi ∨ \psi)$. Its undischarged assumptions are those of $D$.

\vspace{1mm}

Sequent Rule $(∨E)$
: 

If $(Γ ∪ {φ} \vdash χ)$ and $(∆ ∪ {ψ} \vdash χ)$ are correct sequents,
then the sequent $(Γ ∪ ∆ ∪ {(φ ∨ ψ)} \vdash χ)$ is correct.

\vspace{1mm}

Natural Deduction Rule $(∨E)$
: 

Given derivations
\AxiomC{$D$} \noLine
\UnaryInfC{$(\phi ∨ \psi)$}
\DisplayProof
,
\AxiomC{$D\prime$} \noLine
\UnaryInfC{$\chi$}
\DisplayProof 
, and
\AxiomC{$D\prime\prime$} \noLine
\UnaryInfC{$\chi$}
\DisplayProof 
we have derivation
\begin{prooftree}

\AxiomC{$D$} \noLine
\UnaryInfC{$(\phi ∨ \psi)$}

\AxiomC{$\cancel\phi$} \noLine
\UnaryInfC{$D\prime$} \noLine
\UnaryInfC{$\chi$}

\AxiomC{$\cancel\psi$} \noLine
\UnaryInfC{$D\prime\prime$} \noLine
\UnaryInfC{$\chi$}

\RightLabel{$(∨E)$}
\TrinaryInfC{$\chi$}

\end{prooftree}

Its undischarged assumptions are those of $D$,  those of $D\prime$ except possibly $\phi$, and those of $D\prime\prime$ except possible $\phi$.

\vspace{1mm}

\vspace{1mm}

---
author: Keane Yahn-Krafft
date: July 4, 2017
lang: en
papersize: a4
fontsize: 13pt
documentclass: article
classoption: onecolumn, twoside
geometry: margin=1.5in
header-includes: |
    \usepackage[utf8x]{inputenc}
    \usepackage{cancel}
    \usepackage{bussproofs}
---

How To Prove It: Properties
---------------------------------------


1.1 Validity (of an argument)
: 

An argument is _valid_ if the premises cannot be all true without the conclusion
being true as well.

\vspace{1mm}

1.1.2 Demorgan's Laws
: 

i. $¬(P∧Q) ≡ (¬P) ∨ (¬Q)$

ii. $¬(P∨Q) ≡ (¬P) ∧ (¬Q)$

\vspace{1mm}

1.1.3 Commutavity of AND and OR
: 

i. $P ∧ Q ≡ Q ∧ P$

ii. $P ∨ Q ≡ Q ∨ P$

\vspace{1mm}

1.1.4 Associativity of AND and OR
: 

i. $(P ∧ Q) ∨ R ≡ P ∧ (Q ∧ R)$

ii. $(P ∨ Q) ∨ R ≡ P ∨ (Q ∨ R)$

\vspace{1mm}

1.1.5 Idempotence of AND and OR
: 

i. $P ∧ P ≡ P$

ii. $P ∨ P ≡ P$

\vspace{1mm}

1.1.6 Distributivity of AND and OR
: 

i. $P ∧ (Q ∨ R) ≡ (P ∧ Q) ∨ (P ∧ R)$

ii. $P ∨ (Q ∧ R) ≡ (P ∨ Q) ∧ (P ∨ R)$

\vspace{1mm}

1.1.7 Absorption of AND and OR
: 

i. $P ∨ (P ∧ Q) ≡ P ∧ Q$

ii. $P ∧ (P ∨ Q) ≡ P ∨ Q$

\vspace{1mm}

1.1.8 Double Negation
: 

$¬¬P ≡ P$

\vspace{1mm}

1.1.9 Tautology 
: 

A tautology is a formula that is always true.

\vspace{1mm}

1.1.9a Tautology Laws
: 

Let $T$ be a tautology.

i. $P ∧ T ≡ P$

ii. $P ∨ T$ is a tautology

iii. $¬T$ is a contradiction

\vspace{1mm}

1.1.10 Contradiction
: 

A contradiction is a formula that is always false.

\vspace{1mm}

1.1.10a Contradiction Laws
: 

Let $F$ be a contradiction.

i. $P ∧ F$ is a contradiction

ii. $P ∨ F ≡ P$

iii. $¬F$ is a tautology

\vspace{1mm}

1.2.1 Truth Set
: 

The _truth set_ of a statement $P(x)$ is the set of all values $x$ for which
$P(x)$ is true; i.e., 

    \begin{center}
the truth set of $P(x) = \{ x \mid P(x) \}$.
\end{center}

\vspace{1mm}

1.2.2 Set Intersection
: 

The _intersection_ of two sets $A$ and $B$ is the set $A ∩ B$ defined as
follows:

\begin{center}
$A ∩ B = \{ x \mid (x ∈ A) ∧ (x ∈ B) \}$.
\end{center}

\vspace{1mm}

1.2.3 Set Union
: 

The _union_ of two sets $A$ and $B$ is the set $A ∪ B$ defined as follows:

\begin{center}
$A ∩ B = \{ x \mid (x ∈ A) ∨ (x ∈ B) \}$.
\end{center}

\vspace{1mm}

1.2.3 Set Difference
: 

The _difference_ of two sets $A$ and $B$ is the set $A \ B$ defined as follows:

\begin{center}
$A \ B = \{ x \mid (x ∈ A) ∨ (x \notin B) \}$.
\end{center}

\vspace{1mm}

1.2.4 Subset
: 

Given two sets $A$ and $B$, we write $A ⊆ B$ meaning that $A$ is a subset of $B$; that is, 

\begin{center}
$∀x(x ∈ A →  x ∈ B)$.
\end{center}

\vspace{1mm}

If $A$ and $B$ share no elements, they are _disjoint_; that is,

\begin{center}
$∀x((x ∈ A → x ∈ B) ∧ (x ∈ B → x ∈ A))$.
\end{center}

\vspace{1mm}

1.3.1 Conditional Laws
: 

i. $P → Q ≡ (¬P) → Q$

ii. $P → Q ≡ ¬(P ∨ (¬Q))$

\vspace{1mm}

1.3.2 Contrapositive Law
: 

$P → Q ≡ (¬Q) → (¬P)$

\vspace{1mm}

2.1.1 Quantifier Laws
: 

i. $¬∃xP(x) ≡ ∀x¬P(x)$

ii. $¬∀xP(x) ≡ ∃x¬P(x)$

**Note:** that by the double negation law, $¬¬P(x) ≡ P(x)$, and so,
taking $P(x) = ∀x∈ A Q(x)$, we have $¬¬∀x∈ A Q(x) ≡ ¬∃x∈ A ¬Q(x) ≡ ∀x∈ A Q(x)$,
by applying the double negation law once and twice respectively.


\vspace{1mm}

2.2.1 Set Membership
: 

$x ∈ \{ n^2 \mid n ∈ \mathbbalt{N} \} ≡ ∃n∈ \mathbbalt{N} (n^2)$

\vspace{5mm}

2.1.2 Unique Existence
: 

$∃!xP(x) ≡ ∃x(P(x) ∧ ¬∃y(P(y) ∧ y ≠ x))$.

\vspace{5mm}

2.2.2 Power Set
: 

Suppose $A$ is a set. The _power set_ of $A$, denoted $\mathscr{P}(A)$, is the
set that contains all subsets of A. That is to say,

\begin{center}
$\mathscr{P}(A) = \{ x \mid x ⊆ A \}$.
\end{center}

\vspace{1mm}

2.2.3 Intersection and Union of Sets
: 

Suppose $\mathscr{F}$ is a family of sets. Then the _intersection_ and _union_
of $\mathscr{F}$ are the sets $∩ \mathscr{F}$ and $∪ \mathscr{F}$ defined as
follows:

\begin{center}
$∩\mathscr{F} = \{ x \mid ∀A ∈ \mathscr{F} (x ∈ A) \} = \{ x \mid ∀A(A ∈ \mathscr{F} → x ∈ A )\}$ 

and

$∪\mathscr{F} = \{ x \mid ∃A ∈ \mathscr{F} (x ∈ A) \} = \{ x \mid ∃A(A ∈ \mathscr{F} → x ∈ A )\}$.
\end{center}

\vspace{1mm}

3.1.1 Cartesian Product
: 

Suppose $A$ and $B$ are sets. Then the _Cartesian product_ of $A$ and $B$,
denoted $A \times B$, is the set of all ordered pairs in which the first
coordinate is an element of $A$ and the second is an element of $B$; that is,

\begin{center}
$A \times B = \{ (a,b) \mid (a ∈ A) ∧ (b ∈ B) \}$.
\end{center}

\vspace{1mm}

3.1.2 Truth Set of Cartesian Product
: 

As with truth sets for propositions of one variable, the truth set of a
proposition of an ordered pair is defined as follows:

\begin{center}
truth set of $P(a,b) = \{ (a,b) ∈ A \times B \mid P(a,b) \}$.
\end{center}

\vspace{1mm}

3.2.1 Relations
: 

i. Suppose $A$ and $B$ are sets. Then a set $R ⊆ A \times B$ is called a
_relation from A to B_.

ii. $i_{A}$ is the identity relation defined $i_{A} = \{(x,y) ∈ A \times A \mid x = y \}$.

\vspace{1mm}

3.2.2a Domain, Range, Inverse, and Composition of Relation
: 

Suppose $R$ is a relation from $A$ to $B$. 

i. Then the _domain of_ $R$ is the set

\begin{center}
$Dom(R) = \{ a ∈ A \mid ∃b ∈ B ((a,b) ∈ R) \}$.
\end{center}

ii. The _range_, or _image_, of $R$ is the set

    \begin{center}
    $img\: R = Ran(R) = \{ b ∈ B \mid ∃a ∈ A ((a,b) ∈ R) \}$.
    \end{center}

    Let there be a function $f:\mathbbalt{U} \mapsto \mathbbalt{V}$.
    Now, note that the usually synonymous _range_ and _image_ differ 
    significantly from _codomain_ in that the latter refers to the
    set $V$, whereas the former refer to the subset of $V$ actually 
    mapped to by $f$: that is, the set $img\: f$ or $Ran(f)$ as defined 
    above.

iii. The _inverse_ of $R$ is the relation $R^{-1}$ from $B$ to $A$ defined as follows:

\begin{center}
$R^{-1} = \{ (b,a) ∈ B \times A \mid (a,b) ∈ R \}$.
\end{center}

iv. Suppose $S$ is a relation from $B$ to $C$. Then the _composition_ of $S$ and
$R$ is the relation $S \circ R$ defined as follows:

\begin{center}
$S \circ R = \{ (a,c) ∈ A \times C \mid ∃b ∈ B(((a,b) ∈ R) ∧ ((b,c) ∈ S))\}$.
\end{center}

\vspace{12mm}

3.2.2b Properties Thereof
: 
\begin{align*}
(R^{-1})^{-1} &= R \\
Dom(R^{-1}) &= Ran(R) \\
Ran(R^{-1}) &= Dom(R) \\
T \circ (S \circ R) &= (T \circ S) \circ R \\
(S \circ R)^{-1} &= R^{-1} \circ S^{-1} \\
\end{align*}

\vspace{1mm}

3.2.3a Reflexivity, Symmetry, and Transitivity of Relations
: 

Suppose $R$ is a relation on $A$, that is $R: A → A$ or $R ⊆ A \times A$.

i. $R$ is _reflexive_ if $∀x∈ A(xRx)$, or, equivalently, $∀x∈ A((x,x) ∈ R)$.

ii. $R$ is _symmetric_ if $∀x∈ A ∀y∈ A (xRy → yRx)$.

iii. $R$ is _transitive_ if $∀x∈ ∀y∈ A ∀z∈ A ( (xRy ∧ yRz) → xRz)$.

\vspace{1mm}

3.2.3b Properties Thereof
: 

i. $R$ is _reflexive_ iff $i_{A} ⊆ R$.

ii. $R$ is _symmetric_ iff $R = R^{-1}$.

iii. $R$ is _transitive_ iff $R \circ R ⊆ R$.


\vspace{1mm}

\vspace{1mm}

\vspace{1mm}

\vspace{1mm}

\vspace{1mm}

\vspace{1mm}

\vspace{1mm}

\vspace{1mm}

\vspace{1mm}




---
date: October 10, 2019
title: Vector Calculus (23A) Cheat Sheet
author: Keane Yahn-Krafft
lang: en
papersize: a4
fontsize: 13pt
documentclass: article
classoption: onecolumn, twoside
geometry: margin=1.5in
header-includes: |
    \usepackage{ucs}
    \usepackage{amsthm}
    \usepackage{multirow}
    \usepackage{listings}
    \usepackage{enumitem}
    \usepackage{mathtools}
    \DeclareMathOperator{\taninv}{{\tan}^{-1}}

---
\let\mathbbalt\mathbb

\tableofcontents
\clearpage

\input{../ThmStyle.tex}

\newcommand{\hatb}[1]{\hat{\mathbf{#1}}}
\newcommand{\real}{\mathbbalt{R}}
\newcommand{\nat}{\mathbbalt{N}}
\newcommand\deq{\mathrel{\stackrel{\makebox[0pt]{\mbox{\normalfont\tiny def}}}{=}}}

\makeatletter
\renewcommand*\env@matrix[1][\arraystretch]{%
  \edef\arraystretch{#1}%
  \hskip -\arraycolsep
  \let\@ifnextchar\new@ifnextchar
  \array{*\c@MaxMatrixCols c}}
\makeatother

\newcommand{\nreal}[1]{\mathbbalt{R}^{#1}}
\newcommand{\nnat}[1]{\mathbbalt{R}^{#1}}
\renewcommand{\v}[1]{\vec{\mathbf{#1}}}
\newcommand{\p}[1]{\mathbf{#1}}
\newcommand{\SectionCounter}[1]{ \setcounter{section}{1} \setcounter{subsection}{1} \setcounter{subsubsection}{1} }

# Chapter 1
\setcounter{section}{1}
\setcounter{subsection}{0}

## Points, Vectors, and Miscellaneous Preliminaries

### Points vs Vectors
\begin{defn} Point, vector, and coordinates

The element $\nreal{n}$ with coordinates $x_1,x_2,x_3,...,x_n$ can be interpreted
in two ways: as the point

\begin{center}
$\p{x} = 
\begin{pmatrix}
x_1 \\
\vdots \\
x_n
\end{pmatrix}$,
or as the vector $\v{x} = \begin{bmatrix} x_1 \\ \vdots \\ x_n \end{bmatrix}$,
which represents the increment.
\end{center}

\end{defn}

### Subtraction and Addition of Vectors and Points

\begin{defn} Vectors are added by "zipping" together corresponding
elements and adding each pair. Take vectors $\v{v},\v{w} ∈ \nreal{n}$.
Let

\begin{center}
$\v{v} = \begin{bmatrix} v_1 \\ v_2 \\ \vdots \\ v_n \end{bmatrix}$ and 
$\v{w} = \begin{bmatrix} w_1 \\ w_2 \\ \vdots \\ w_n \end{bmatrix}$, so
$\v{v} + \v{w} = 
\begin{bmatrix} v_1 + w_1 \\ v_2 + w_2 \\ \vdots \\ v_n + w_n \end{bmatrix}$
\end{center}


Note: This is very much like that of the following haskell snippet:
\begin{lstlisting}[language=Haskell]
addVector :: [a] -> [a] -> [a]
addVector = zipWith (+)
\end{lstlisting}
\end{defn}


### Scalar Multiplication of Vectors

\begin{defn}
Let vector $\v{v} ∈ \nreal{n}$ and $r ∈ \real$. Then 

$a\begin{bmatrix} x_1 \\ x_2 \\ \vdots \\ x_n \end{bmatrix} =
\begin{bmatrix} ax_1 \\ ax_2 \\ \vdots \\ ax_n \end{bmatrix}$.

Theoretically the contents of the vectors, and the scalar itself, 
could be parameterized, as long as the replacement is a field.
\end{defn}

### Standard Basis Vectors

\begin{defn} Standard Basis Vectors in $\nreal{n}$

The \emph{standard basis vectors} in $\nreal{n}$ are the vectors $\v{e_j}$ with
$n$ entries, the $j$th entry 1 and the others 0.

For example, in $\nreal{3}$ they are denoted $\v{i}, \v{j}, \v{k}$ or
$\hatb{i}, \hatb{j}, \hatb{k}$, and defined as follows:

\begin{center}
$\v{i} = \v{e_1} = \begin{bmatrix} 1 \\ 0  \\ 0 \end{bmatrix};\quad
\v{j} = \v{e_2} = \begin{bmatrix} 0 \\ 1 \\ 0 \end{bmatrix};\quad
\v{k} = \v{e_3} = \begin{bmatrix} 0 \\ 0 \\ 1 \end{bmatrix}$.
\end{center}

\end{defn}


## Geometry of $\nreal{3}$

\begin{defn} \emph{Dot product}

The \emph{dot product} $\v{x} \cdot \v{y}$ of two vectors $\v{x}, \v{y} ∈
\nreal{n}$ is 

$$ \v{x} \cdot \v{y} =
\begin{bmatrix}
    x_1 \\
    x_2 \\
    \vdots \\
    x_n \\
\end{bmatrix}
  \cdot
\begin{bmatrix}
    y_1 \\
    y_2 \\
    \vdots \\
    y_n \\
\end{bmatrix} 
\deq x_1y_1 + x_2y_2 + ... + x_ny_n.$$
\end{defn}

\begin{defn} \emph{The length of a vector}

    The \emph{length} $|\v{x}|$ of a vector $\v{x} ∈ \nreal{n}$ is
    $$ |\v{x}| \deq \sqrt{\v{x} \cdot \v{x}} = \sqrt{ x_1^2 + x_2^2 + ... + x_n^2}
        \deq \sqrt{ \sum_{i=1}^{n} x_{i}^{2}}.$$
\end{defn}


\begin{thm} \textbf{Geometric interpretation of dot product}

    Let $\v{x}, \v{y}$ in $\nreal{2}$ or $\nreal{3}$, and let $\alpha$ be the angle
    between them. Then
            $$\v{x} \cdot \v{y}  =  |\v{x}| |\v{y}| \cos \alpha.$$
\end{thm}

\begin{defn} \textbf{\emph{The angle between two vectors}}

    The \emph{angle} between two vectors $\v{v},\v{w} ∈ \nreal{n}$ is that
    angle $\alpha$ satisfying $0 ≤ \alpha ≤ π$ such that
            $$\cos \alpha  = \dfrac{\v{x} \cdot \v{y}}{|\v{x}| |\v{y}|}.$$

    Note that as a result 
        $$\alpha  = \arccos \dfrac{\v{x} \cdot \v{y}}{|\v{x}| |\v{y}|}.$$
\end{defn}

\begin{defn} \textbf{\emph{Vector Projection}}

    The projection of a vector, $\vec{v}$, onto another vector, $\vec{w}$, with
    angle $\theta$ between them is:

    $$proj_{\vec{w}}\vec{v} 
        = \frac{|\vec{v} \cdot \vec{w}|}{{\|\vec{w}\|}^2} \vec{w}
        = \frac{|\vec{v} \cdot \vec{w}|}{\|\vec{w}\|} \frac{\vec{w}}{\|\vec{w}\|}
        = (\|\vec{v}\|\cos\theta) \frac{\vec{w}}{\|\vec{w}\|} .$$

    The length of the $proj_{\vec{w}}\vec{v}$ is:

    $$\|proj_{\vec{w}}\vec{v}\|
        = \left\lVert\frac{|\vec{v} \cdot \vec{w}|}{{\|\vec{w}\|}^2} \vec{w}\right\rVert
        = \frac{|\vec{v} \cdot \vec{w}|}{{\|\vec{w}\|}^2} \|\vec{w}\|
        = \frac{|\vec{v} \cdot \vec{w}|}{\|\vec{w}\|}.$$

\end{defn}

\begin{thm} \textbf{The triangle inequality: } For vectors $\v{x}, \v{y} ∈ \nreal{n}$,
    $$|\v{x} + \v{y}| ≤ |\v{x}| + |\v{y}|.$$
\end{thm}

\begin{defn} \textbf{Determinant in $\nreal{2}$}

    The \emph{determinant} det of a $2 \times 2$ matrix 
    $\begin{bmatrix} a_1 & b_1 \\ a_2 & b_2 \end{bmatrix}$ is given by
        \begin{center}
            det $\begin{bmatrix} a_1 & b_1 \\ a_2 & b_2 \end{bmatrix}
                    \deq a_1b_2 - a_2b_2.$
        \end{center}
\end{defn}

\begin{defn} \textbf{\emph{Cauchy–Schwarz Inequality}}
For any two vectors a and b, we
have

\begin{center}
$|a · b| ≤ \|a\| \|b\|$
\end{center}

with equality if and only if a is a scalar multiple of b, or one of them is 0.
\end{defn}

\begin{defn}
    The \emph{determinant}

    \begin{center}
    $\begin{vmatrix}
      a_{11} & b_{12}  \\
      d_{12} & e_{22}  \\
    \end{vmatrix}$
    \end{center}

    of such a matrix is the real number defined by the equation

    \begin{center}
    $\begin{vmatrix}
      a_{11} & a_{12}  \\
      a_{21} & a_{22}  \\
    \end{vmatrix}= a_{11}a_{22} - a_{12}a_{21}$.
    \end{center}


\end{defn}

\begin{defn}

    The \emph{determinant} of a $3 \times 3$
    matrix is defined by the rule

    \begin{center}
    $\begin{vmatrix}
      a_{11} & a_{12} & a_{13} \\
      a_{21} & a_{22} & a_{23} \\
      a_{31} & a_{32} & a_{33} \\
    \end{vmatrix}= 
    a_{11}
    \begin{vmatrix}
      a_{22} & a_{23} \\
      a_{32} & a_{33} \\
    \end{vmatrix}
    - a_{12}
    \begin{vmatrix}
      a_{21} & a_{23} \\
      a_{31} & a_{33} \\
    \end{vmatrix}
    + a_{13}
    \begin{vmatrix}
      a_{21} & a_{22} \\
      a_{31} & a_{32} \\
    \end{vmatrix}$.
    \end{center}

\end{defn}

\begin{defn} \textbf{Cross Product}

    \begin{defn} \textbf{\emph{The Cross Product}}

    Suppose that $\vec{a} = a_1\hatb{i} + a_2\hatb{j} + a_3\hatb{k}$ and $\vec{b} = b_1\hatb{i}
    + b_2\hatb{j} + b_3\hatb{k}$ are vectors in ${\mathbbalt{R}}^3$. 

    The \emph{cross product} or \emph{vector product} of $\vec{a}$ and $\vec{b}$, 
    denoted $\vec{a} \times \vec{b}$, is defined to be the vector

    \begin{center}
    $\vec{a} \times \vec{b} =
    \begin{vmatrix}
      \hatb{i} & \hatb{j} & \hatb{j} \\
      a_1 & a_2 & a_3 \\
      b_1 & b_2 & b_3 \\
    \end{vmatrix} =
    \hatb{i}
    \begin{vmatrix}
      a_2 & a_3 \\
      b_2 & b_3 \\
    \end{vmatrix} 
    - \hatb{j}
    \begin{vmatrix}
      a_1 & a_3 \\
      b_1 & b_3 \\
    \end{vmatrix} 
        + \hatb{k} 
    \begin{vmatrix}
      a_1 & a_2 \\
      b_1 & b_2 \\
    \end{vmatrix}$.
    \end{center}
    \end{defn}
    \vspace{6mm}

    \begin{defn} \textbf{\emph{The Cross Product}}

    \emph{Geometric Definition} : $\vec{a} \times \vec{b}$ is the vector such that:

    \begin{enumerate}
    \item{
    $\|\vec{a} \times \vec{b}\| = \|\vec{a}\| \|\vec{b}\| \sin θ$, the area of
    the parallelogram defined by $\vec{a}$ and $\vec{b}$; $0 ≤ θ ≤ \pi$ 
    is the angle between $\vec{a}$ and $\vec{b}$.}

    \item{
    $\|\vec{a} \times \vec{b}\|$ is perpendicular to $\vec{a}$ and $\vec{b}$, and
    the triple $(\vec{a}, \vec{b}, \vec{a} \times \vec{b})$ obeys the right-hand
    rule.}
    \end{enumerate}

    \emph{Component Formula}:
    \begin{center}
    $\vec{a} \times \vec{b} =
    \begin{vmatrix}
      \hatb{i} & \hatb{j} & \hatb{j} \\
      a_1 & a_2 & a_3 \\
      b_1 & b_2 & b_3 \\
    \end{vmatrix} = 
    (a_2b_3 - a_3b_2)\hatb{i} - (a_1b_3 - a_3b_1)\hatb{j} + (a_1b_2 a_2b_1)\hatb{k}$.
    \end{center} 

    \emph{Alegbraic Rules}:
    \begin{enumerate}

    \item $\vec{a} \times \vec{b}$ iff $\vec{a}$ and $\vec{b}$ are parallel or
    $\vec{a}$ or $\vec{b}$ is zero.

    \item $\vec{a} \times \vec{b}  -\vec{b} \times \vec{a}$

    \item $\vec{a} \times (\vec{b} \times \vec{c}) =  \vec{a} \times \vec{b} + \vec{a} \times \vec{c}$

    \item $(\vec{a} \times \vec{b}) \times \vec{c} =  \vec{a} \times \vec{c} + \vec{b} \times \vec{c}$

    \item $(α\vec{a}) \times \vec{b} = α (\vec{a} \times \vec{b})$

    \end{enumerate}

    \emph{Multiplication Table}:


    \centering
    \def\arraystretch{1.5}
    $\begin{tabular}{cc|rrr}
     \multirow{2}{*}{}
     & & \multicolumn{3}{l}{Second factor} \\
     & $\times$               & $\hatb{i}$ & $\hatb{j}$ & $\hatb{k}$  \\ 
    \cline{2-5}
     \multirow{3}{*}{First factor}
     & $\hatb{i}$             & $\hatb{0}$  & $\hatb{k}$   & $\hatb{-j}$   \\
     & $\hatb{j}$             & $\hatb{-k}$ & $\hatb{0}$   & $\hatb{i}$   \\
     & $\hatb{k}$             & $\hatb{j}$  & $\hatb{-i}$  & $\hatb{0}$ \\
    \end{tabular}$

    \end{defn}



\end{defn}

\begin{defn} \textbf{The cross product in $\nreal{3}$}

    The \emph{cross product} $\v{a} \times \v{b}$ in $\nreal{3}$ is
    \begin{center}
        $\begin{bmatrix} a_1 \\ a_2 \\ a_3 \end{bmatrix} \times
        \begin{bmatrix} b_1 \\ b_2 \\ b_3 \end{bmatrix} \deq
        \begin{bmatrix}[1.4]
                \phantom{-}$det $ 
                \begin{bmatrix}
                  a_2 & b_2 \\
                  a_3 & b_3
                \end{bmatrix} \\
                - $det $ 
                \begin{bmatrix} 
                  a_1 & b_1 \\
                  a_3 & b_3
                \end{bmatrix} \\
                \phantom{-}$det $ 
                \begin{bmatrix}
                  a_2 & b_2 \\
                  a_3 & b_3
                \end{bmatrix}
        \end{bmatrix} =
        \begin{bmatrix*}[r]
            a_2b_3 - a_3b_2 \\ 
           -a_1b_3 + a_3b_1 \\ 
            a_1b_2 - a_2b_1 
        \end{bmatrix*}
        $
    \end{center}
\end{defn}

\begin{thm} The cross product and the determinant satisfy
    \[\emph{det }[\v{a}, \v{b}, \v{c}] = \v{a} \cdot (\v{b} \times \v{c}).\]

    N.B. This is the volume of the parallelpiped formed by $a$, $b$, and $c$.
\end{thm}


## Lines and Planes

\begin{defn} \textbf{Parallel Lines}

    If two nonzero vectors, $\v{v}$ and $\v{u}$, are parallel then there must be a
    scalar, $k$, such that $\v{u} = k\v{v}$. When $\v{u}$ and $\v{v}$ have the
    same directon $k = \frac{\|\v{u}\|}{\|\v{v}\|}$. If they have opposite
    directions, negate this quotient.
\end{defn}

\begin{defn} \textbf{Lines in Space}

    Let $L$ be a line in $\nreal{n}$ described by a point, $P \in \nreal{n}$
    and a vector parallel to the line, $\v{v} \in \nreal{n}$.  Let $P = (x_{0},
    y_{0}, z_{0})$ and $\v{v} = \langle a, b, c \rangle$. For any point $Q =
    (x,y,z)$ on $L$ we have that $\overrightarrow{PQ}$ is parallel to $\v{v}$,
    so there is a scalar, $t$, such that $\overrightarrow{PQ} = t\v{v}$.  Let
    $\v{r} = \langle x,y,z \rangle$ and $\v{r_0} = \langle x_0, y_0, z_0
    \rangle$.  Then $$\v{r} = \v{r_0} + t\v{v}.$$

    Solving for each component of $\v{r}$ yields a set of equations defined in
    terms of $t$ that describe $L$.

    We can specify a \emph{segment} of $L$ by restricting $t$ to $a \leq t \leq
    b$. 

    Alternatively, we could specify a segment, $\overrightarrow{RS}$ between two
    points (with their associated vectors), $R$ and $S$.  Let $\vec{x} =
    \langle x,y,z \rangle$. Then we have

    \begin{align*}
    \vec{x} & = \vec{r} + t\overrightarrow{RS}  \\
            & = \vec{r} + t\vec{s} - t\vec{r} \\
            & = (1 - t)\vec{r} + t\vec{s}
    \end{align*}


    Which equation describes exactly the segment we wanted for $0 \leq t \leq 1$.

    Say we wanted to express the same segment, $\overrightarrow{RS}$, but where
    $a \leq t \leq a + b$, $x = R$ at $t = a$, and $x = S$ at $t =a + b$. We can describe the 
    segment with this new parameterization with the given equation:

    \begin{align*}
    \vec{x} & = \vec{r} + \frac{(t-a)}{b}\overrightarrow{RS}  \\
            & = \vec{r} + \frac{(t-a)}{b}\vec{s} - \frac{(t-a)}{b}\vec{r} \\
            & = (1 - \frac{(t-a)}{b})\vec{r} + \frac{(t-a)}{b}\vec{s}
    \end{align*}

    Thus, when $t=a$, $x = r$, as the coefficient of $\vec{r}$ is $1$ and that
    of $\vec{s}$ $0$; likewise, when $t = a + b$, $x = s$.

\end{defn}

\begin{defn} \textbf{Distance from a Point to a Line}
    
    Let $L$ be a line in space passing through point $P$ with direction vector
    $\v{v}$. If $M$ is any point not on $L$, then the distance from $M$ to $L$
    is $$d = \frac{\|\overrightarrow{PM} \times \v{v}\|}{\|\v{v}\|}$$.  
    Note that the area of a parallelogram is the product of its base and
    height.

\end{defn}

\begin{defn} \textbf{\emph{Distance Between a Point and a Plane}}

    Suppose a plane with normal vector $\vec{n}$ passes through point Q. The
    distance between the plane to point P not in the plane is given by
    $$d = \|proj_{\vec{n}}\overrightarrow{QP}\| = comp_{\vec{n}}\overrightarrow{QP} = \frac{|\overrightarrow{QP}\cdot\vec{n}|}{\|\vec{n}\|}
    = \|\overrightarrow{QP}\|\cos\theta$$

    where $\theta$ is the angle between $\vec{n}$ and
    $\overrightarrow{QP}$.
\end{defn}

\begin{defn} \textbf{\emph{The angle between two planes}}
    Given two planes with angle $\theta$ between them and normal vectors $v$
    and $w$, respectively, the angle between these planes is given by:
    $$\cos{\theta} = \frac{ |\vec{v} \cdot \vec{w}| }{ \|\vec{v}\|\|\vec{w}\|
    }.$$
    Let $\psi$ be the supplementary angle of $\theta$, that is, such that $\psi +
    \theta = \pi$. Then this formula wil always yield $min(\psi,
    \theta)$.
\end{defn}

## Quadratic Surfaces

All of the below surface equations can be derived from the general form for
$\nreal{3}$:

$$Ax^2 + By^2 + Dxy + Exz + Fyz + Gx + Hy + Jz + K = 0.$$

![Quadratic Surfaces (part1)](assets/QuadraticSurfaces1.jpeg)
![Quadratic Surfaces (part2)](assets/QuadraticSurfaces2.jpeg)



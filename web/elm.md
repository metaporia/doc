# _Beginning Elm_ Notes

[source](https://elmprogramming.com/)

## Neovim config

1. get `npm` via `nvm`

2. `npm install -g elm elm-test elm-oracle elm-format`

3. add `Plug 'Zaptic/elm-vim', {'for':'elm'}` to `init.vim`

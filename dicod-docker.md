# Setup dockerized dico (DICT) server

1. Install docker.

On Arch Linux run:
```bash
sudo pacman -S docker --noconfirm
# OR,
aurman -S docker --noconfirm
```

Add user to docker group:
```bash
sudo usermod -aG docker $USER
```

Logout to effect changes:
```bash
kill -9 -1
```

Test docker daemon:
```bash
docker ps
# OR,
docker run hello-world
```


2. Download dicod-docker image from DockerHub:

```bash
docker pull beryj7/dicod-docker
```


3. Add service file (for systemd).

Place ~/dot/dicod-docker.service (git@gitlab.com:metaporia/dot.git), or the
below at /lib/systemd/system/dicod-docker.service

```
[Unit]
Description=Dockerized GNU Dico DICT server

[Service]
ExecStart=/home/aporia/scripts/dicod-docker

[Install]
WantedBy=multi-user.target
```

```bash
sudo ln -s ~/dot/dicod-docker.service /lib/systemd/system/
```

Then enable and start the service with:
```bash
sudo systemctl enable dicod-docker
sudo systemctl start dicod-docker
```

4. Install the dico client.

On Arch Linux, run:
```bash
sudo pacman -S dico --noconfirm
```

Or, since the AUR package has unmet dependencies at the time of writing,
download and compile manually [GNU Dico 2.7](ftp://download.gnu.org.ua/pub/release/dico/dico-2.7.tar.gz).

```bash
wget ftp://download.gnu.org.ua/pub/release/dico/dico-2.7.tar.gz
tar -xf dico-2.7.tar.gz
cd dico-2.7
./configure
make -j4
sudo make install
```

5. Configure dico client to query local dico instance.

Link ~/dot/.dico, or the below, to ~/.dico.

```
# dico init file

# disable welcome banner
quiet yes

# default server
open localhost

# search in all dbs
database *

# custom command prefix
prefix :
```


```bash
ln -s ~/dot/.dico ~/
```




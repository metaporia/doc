# tex # 
---------------------------------------
## hello world

create a `hello_world.tex` file with the following contents:

    \documentclass{article}
    \begin{document}
    A \textbf{bold \textit{Hello \LaTeX}} to start!
    =end{document}

latex 'code' (macro statements, plain text) compiles w LaTeX, or other TeX engine, e.g.,

    $ pandoc hello_world.tex -o hello_world.pdf # this works


`\<macro>`s may be nested as evinced above 


\documentclass
:   'article' | 'letter' | 'book' 
    predefined feature sets

\begin
:   denote start of document body  

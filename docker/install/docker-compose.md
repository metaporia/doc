## Install `docker-compose`

via curl:

```bash
sudo curl -L https://github.com/docker/compose/releases/download/1.21.2/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose

sudo chmod +x /usr/local/bin/docker-compose
```

or via pip:
```bash
sudo pip install docker-compose
```



NB: check the [repo](https://github.com/docker/compose/releases) for a more
up-to-date version

# Pixel Saver installation notes (gnome-shell v3.3.0)

See [repo](https://github.com/deadalnix/pixel-saver#installation) for installation instructions.

Commit d97c6e4 worked (the latest on master) an 6/8/18.

Run the following:

```bash
git clone https://github.com/deadalnix/pixel-saver.git

cd pixel-saver

# Get the last released version--in this case, master. v3.3.0 support hasn't 
# been merged into a release branch yet.
#git checkout 1.90

# copy to extensions directory
cp -r pixel-saver@deadalnix.me -t ~/.local/share/gnome-shell/extensions

# activate
gnome-shell-extension-tool -e pixel-saver@deadalnix.me
```

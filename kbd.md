# Gnome Keyboard-Shortcut Workaround

Within the last week (it's 19/10/09), some keyboard settings have changed such
that Gnome now requires a restart after each system reboot for custom
keyboard-shortcuts to respect dvorak. For example, when I press ctrl-alt-k,
which calls ~/scripts/raise_tmux, nothing happens; ctrl-alt-t, however, works.
These bindings are written with respect to the qwerty legends, so this _not_
the desired behavior: ctrl-alt-t (in dvorak) should trigger raise_tmux.


1. Inspect current keyboard settings:

```
setxkbmap  -print -verbose 10
gsettings get org.gnome.desktop.input-sources sources
gsettings get org.gnome.desktop.input-sources xkb-options
```

2. Configure directly:

```
setxkbmap -model pc105 -layout us,us -variant dvorak, -option ctrl:nocaps terminate:ctrl_alt_bkspc
```

3. Disable Gnome keyboard configuration override:

```
```
https://medium.com/@damko/a-simple-humble-but-comprehensive-guide-to-xkb-for-linux-6f1ad5e13450


self: super:

{
  # Uses fork at gitlab.com/metaporia/not-tetris with dvorak-friendly bindings.
  nottetris2 = super.nottetris2.overrideAttrs (oldAttrs: rec {
    version =  "2.0";
    pname = "nottetris2";
    src = super.fetchFromGitLab {
      owner = "metaporia";
      repo = "not-tetris";
      rev = "7c7fb3e2db949feb14fc20e214b1da7ffb70bc0f";
      sha256 = "0mdq2hv2z7ya17wm7423yr61fwjg55viikghsa5lz2j3q16xk2fx";
    };
  });
}

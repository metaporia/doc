# Nix Overlays

For an example overlay see ./nottetris

It can be testing using ./nottetris/shell.nix

To write a new overlay for somePackage, write
/\$HOME/dot/nix-overlays/somePackage/default.nix containing the overlay. Then
run `home-manager switch`. Then add `overlays.somePackage = import
./somePackage` and then edit the returned list of overlays to include
`somePackage`, e.g., `with overlays; [ ... somePackage ... ]`.


# NixOS Post-Installation Notes

# But, first, one pre-installation note: make suer to symlink dot/systme to
# /etc/nixos 

# First update the channel to generate programs.sqlite (I think). At least
# that's what seems to sort out the pesky "DBI connect ..." error.

sudo nix-channel --update

## Home Manager
# Next, clone your dotfiles and install them with home-manager.

cd ~
git clone https://gitlab.com/metaporia/dot

cd dot
git checkout nixos

# Symlink ~/dot to ~/.config/nixpkgs
ln -s ~/dot ~/.config/nixpkgs


# Remove confliction generated config files
rm ~/.config/fish/config.fish

# Build with home-manager
home-manager switch

# add home-manager channel (it works for a bit without this but inevitably breaks
# saying it can't find home-manager on $NIX_PATH. Perhaps it breaks only once
# it tries to manage itself?)
nix-channel --add https://github.com/nix-community/home-manager/archive/master.tar.gz home-manager
nix-channel --update

# Now we have a basic environment with firefox, alacritty, tmux, nvim, fish.

# Further setup needed:
# - sign in to firefox
# - run :PlugInstall in neovim

# link /bin/bash to nix's bash so as to not break every shebang
sudo ln -s `which bash` /bin/bash

# TODO
# - set timezone lmao
# - make package for modified not-tetris-2 with dvorak support
# - raise_* wrapper that stores full X window titles to fix summoning shortcuts
#   when window titles overlap
# - ssh keys for gitlab, github

# on gnome dpi and font scaling needs to be manually configured

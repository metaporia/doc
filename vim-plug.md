# Vim Plug

Neovim setup:

```
curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

```

Also, run `pip install neovim --user` for neovim python support.

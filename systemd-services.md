# Systemd service

To, for instance, create a service that runs on boot, whether or not a daemon:

1. create a `myservice.service` according to the following template:

```service
[Unit]
Description=My service

[Service]
ExecStart=/path/to/exe

[Install]
WantedBy=multi-user.target 
```

2. set appropriate permissions:

```bash
sudo chmod 755 /usr/bin/my-script
```

3. finally, enable the service

```bash
sudo systemctl enable myscript.service
```

(For a more detailed example see `~/doc/dicod-docker.md` or its
[repository](https://gitlab.com/metaporia/dicod-docker).)
